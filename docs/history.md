# Release history

## version 1.0.x (champagnac)

_The objectif of this release is to stabilize the full repository content and offer a stable release of sxquotas._

## version 0.4.x (chaumeil)

_The objectif of this release is to benefit from feedback and focus on creating a small community rhythm._

## version 0.3.x (chassagne)

_The objectif of this release is to communicate about the availability of this application._

## version 0.2.x (chastang)

The objectif of this release is to provide integration with the kubectl krew plugin mechanism, as well
as rpm, deb, homebrew, snap and container version.

### History

| Release                                                        | Date       | Description                  |
| -------------------------------------------------------------- | ---------- | ---------------------------- |
| [0.2.1](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.2.1) | 2024-11-12 | Upgrade to go version 1.23.3 |

## version 0.1.x (chauzu)

The objectif of this release is to use a common startx library for both sxquotas and sxlimits.

### History

| Release                                                        | Date       | Description                                          |
| -------------------------------------------------------------- | ---------- | ---------------------------------------------------- |
| [0.1.3](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.1.3) | 2024-11-05 | Upgrade all packages for security fix                |
| [0.1.1](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.1.1) | 2024-10-13 | Update all packages and upgrade to sxlib v0.1.11     |
| [0.1.0](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.1.0) | 2024-10-13 | Full update of all packages and move to sxlib v0.1.7 |


## version 0.0.x (champeaux)

This version is a POC for testing purpose.

The objectif of this release is to create the repository structure and fundamentals of the project.

### History

| Release                                                          | Date       | Description                                                                                                          |
| ---------------------------------------------------------------- | ---------- | -------------------------------------------------------------------------------------------------------------------- |
| [0.0.33](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.33) | 2024-10-07 | Release static binary                                                                                                |
| [0.0.31](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.31) | 2024-10-05 | Update all dependencies packages to latest                                                                           |
| [0.0.29](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.29) | 2024-10-03 | Update packages, add Delete for RQ and add sxcollector template                                                      |
| [0.0.27](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.27) | 2024-09-25 | Update all dependencies packages to latest                                                                           |
| [0.0.25](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.25) | 2024-09-11 | Update the go version to 1.23.1                                                                                      |
| [0.0.23](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.23) | 2024-09-07 | Use sxlib v0.0.9                                                                                                     |
| [0.0.21](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.21) | 2024-06-03 | Use the --namespace flag verywhere                                                                                   |
| [0.0.20](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.20) | 2024-06-01 | Display content of the template when using the template subcommand. Adding example-sxquotas.yaml                     |
| [0.0.19](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.19) | 2024-05-04 | Adjust fix from the sxlimits and go-lib projects                                                                     |
| [0.0.17](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.17) | 2024-05-03 | Move all dependencies for common startx libs to module `gitlab.com/startx1/k8s/go-libs@v0.0.7`                       |
| [0.0.15](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.15) | 2024-05-03 | Add support for the describe subcommand. Like export but for terminal display                                        |
| [0.0.13](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.13) | 2024-03-31 | Improve export subcommand with no-header, output and separtor options                                                |
| [0.0.11](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.11) | 2024-04-25 | Adding global command parser with --namespace and --kubeconfig available for command interfacting with the cluster   |
| [0.0.7](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.5)   | 2024-04-13 | first stable release with most of the command present and functional. No support for percentage in adjust and resize |
| [0.0.1](https://gitlab.com/startx1/k8s/sxquotas/-/tags/v0.0.1)   | 2024-04-11 | Initial package                                                                                                      |
