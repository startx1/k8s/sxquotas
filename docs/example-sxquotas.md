# The example project

In order to test the sxquotas behavior, you can create the `test-sxquotas` 
namespace with an example workload by using the following script. Be sure to be connected to 
a kubernetes cluster before executing it.

## Content of the example-sxquotas

The [example-quotas.yaml](./example-sxquotas.yaml) allow you to create the following resource in your current namespace :

- 1 **httpd-small** deployment with 2 replicas
- 1 **httpd-small** service
- 1 **httpd-big** deployment with 1 replica and PVC
- 1 **httpd-big** service
- 1 **httpd-big** PVC using default storageClass
- 1 **cron-5m** cronjob scheduled every 5 minutes
- 5 empty Secrets
- 8 empty ConfigMap

## Installing the example project

```bash
NS=test-sxquotas
kubectl create ns ${NS} && \
kubectl -n ${NS} create -f https://gitlab.com/startx1/k8s/sxquotas/-/raw/devel/docs/example-sxquotas.yaml && \
kubectl -n ${NS} get secret,cm,cronjob,deployment,pod -o wide && \
echo "END creating ${NS}"
```

should return the following result 

```bash
deployment.apps/httpd-small created
service/httpd-small created
deployment.apps/httpd-big created
service/httpd-big created
cronjob.batch/cron-5m created
secret/secret1 created
secret/secret2 created
secret/secret3 created
secret/secret4 created
secret/secret5 created
configmap/configmap1 created
configmap/configmap2 created
configmap/configmap3 created
configmap/configmap4 created
configmap/configmap5 created
configmap/configmap6 created
configmap/configmap7 created
configmap/configmap8 created
persistentvolumeclaim/httpd-big-pvc created
```
