# Installation

## 0. Requirements

- Having a RedHat like (Fedora, CentOS, RHEL, ...) operating system
- Install kubectl with `sudo yum install kubectl`

## 1. Get a copy of sxquotas

```bash
curl https://gitlab.com/startx1/k8s/sxquotas/-/raw/stable/bin/sxquotas -o sxquotas
cp sxquotas /usr/local/bin/
chmod ugo+x /usr/local/bin/sxquotas
```

## 2. Install example namespace

In order to test the sxquotas behavior, you can create the `test-sxquotas` 
namespace with an example workload by using the following script. 
Follow the Example [SXQuotas installation guide](./example-sxquotas.md) instructions.
