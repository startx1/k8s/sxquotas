# sxquotas stack-basic template

Stackable template with basic quotas definitions (cm, secret)

## Usage

```bash
sxquotas create stack-basic-quotas stack-basic
```

## Example output

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-basic-quotas
spec:
  hard:
    secrets: "5"
    configmaps: "5"
```