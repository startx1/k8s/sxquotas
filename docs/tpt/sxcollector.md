# sxquotas sxcollector template

Template designed for a sxcollector resourceQuota definition

## Usage

```bash
sxquotas create sxcollector-quotas sxcollector
```

## Example output

```yaml
apiVersion: v1
  kind: ResourceQuota
  metadata:
    name: full-quotas
  spec:
    hard:
      limits.cpu: "100000m"
      limits.memory: "10Ti"
      limits.ephemeral-storage: "10Ti"
      requests.cpu: "100000m"
      requests.memory: "10Ti"
      requests.ephemeral-storage: "10Ti"
      requests.storage: "10Ti"
      ephemeral-storage: "10Ti"
      persistentvolumeclaims: "1000"
      count/persistentvolumeclaims: "1000"
      count/secrets: "1000"
      count/configmaps: "1000"
      count/deployments.apps: "1000"
      count/replicasets.apps: "1000"
      count/statefulsets.apps: "1000"
      count/daemonsets.apps: "1000"
      count/jobs.batch: "1000"
      count/cronjobs.batch: "1000"
      count/services: "1000"
      services: "1000"
      services.loadbalancers: "1000"
      services.nodeports: "1000"
      pods: "1000"
```