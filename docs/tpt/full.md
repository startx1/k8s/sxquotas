# sxquotas full template

Template designed for a full resourceQuota definition

## Usage

```bash
sxquotas create full-quotas full
```

## Example output

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: full-quotas
spec:
  hard:
    limits.cpu: "2000m"
    limits.memory: "2Gi"
    requests.cpu: "1000m"
    requests.memory: "1Gi"
    requests.storage: "1Gi"
    persistentvolumeclaims: "1"
    ephemeral-storage: "1Gi"
    my-example.storageclass.storage.k8s.io/requests.storage: "1Gi"
    my-example.storageclass.storage.k8s.io/persistentvolumeclaims: "1"
    openshift.io/imagestreams: "2"
    secrets: "2"
    configmaps: "2"
    count/daemonsets.apps: "2"
    count/deployments.apps: "2"
    count/replicasets.apps: "4"
    replicationcontrollers: "1"
    pods: "8"
    services: "3"
    services.loadbalancers: "0"
    services.nodeports: "0"
```