# sxquotas stack-openshift template

Stackable template with openshift quotas definitions (imagestream, build)

## Usage

```bash
sxquotas create stack-openshift-quotas stack-openshift
```

## Example output

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-openshift-quotas
spec:
  hard:
    openshift.io/imagestreams: "5"
```