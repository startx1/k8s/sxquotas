# sxquotas k8s template

Template designed for a basic kubernetes install

## Usage

```bash
sxquotas create k8s-quotas k8s
```

## Example output

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: k8s-quotas
spec:
  hard:
    limits.cpu: "6000m"
    limits.memory: "6Gi"
    requests.cpu: "3000m"
    requests.memory: "3Gi"
    requests.storage: "5Gi"
    persistentvolumeclaims: "5"
    ephemeral-storage: "5Gi"
    secrets: "20"
    configmaps: "10"
    count/deployments.apps: "2"
    pods: "10"
    services: "3"
    services.loadbalancers: "0"
    services.nodeports: "0"
```