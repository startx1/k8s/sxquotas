# sxquotas openshift template

Template designed for a basic openshift install

## Usage

```bash
sxquotas create openshift-quotas openshift
```

## Example output

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: openshift-quotas
spec:
  hard:
    limits.cpu: "6000m"
    limits.memory: "6Gi"
    requests.cpu: "3000m"
    requests.memory: "3Gi"
    requests.storage: "5Gi"
    persistentvolumeclaims: "5"
    ephemeral-storage: "5Gi"
    thin.storageclass.storage.k8s.io/requests.storage: "0Gi"
    thin.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
    thin-csi.storageclass.storage.k8s.io/requests.storage: "0Gi"
    thin-csi.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
    openshift.io/imagestreams: "5"
    secrets: "20"
    configmaps: "10"
    count/daemonsets.apps: "1"
    count/deployments.apps: "2"
    count/replicasets.apps: "6"
    replicationcontrollers: "0"
    pods: "10"
    services: "3"
    services.loadbalancers: "0"
    services.nodeports: "0"
```