# sxquotas stack-network template

Stackable template with network quotas definitions (svc)

## Usage

```bash
sxquotas create stack-network-quotas stack-network
```

## Example output

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-network-quotas
spec:
  hard:
    services: "3"
    services.loadbalancers: "0"
    services.nodeports: "0"
```