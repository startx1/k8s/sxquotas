# sxquotas default template

Default template with basic quotas definitions

## Usage

```bash
sxquotas create default-quotas default
```

## Example output

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: default-quotas
spec:
  hard:
    limits.cpu: "6000m"
    limits.memory: "6Gi"
    requests.cpu: "3000m"
    requests.memory: "3Gi"
    requests.storage: "5Gi"
    persistentvolumeclaims: "5"
    ephemeral-storage: "5Gi"
    secrets: "20"
    configmaps: "10"
    count/daemonsets.apps: "1"
    count/deployments.apps: "2"
    count/replicasets.apps: "6"
    pods: "10"
    services: "3"
    services.loadbalancers: "0"
    services.nodeports: "0"
```