# sxquotas stack-minimal template

Stackable template with minimal quotas definitions (request & limits)

## Usage

```bash
sxquotas create stack-minimal-quotas stack-minimal
```

## Example output

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-minimal-quotas
spec:
  hard:
    limits.cpu: "6000m"
    limits.memory: "6Gi"
    requests.cpu: "3000m"
    requests.memory: "3Gi"
```