# sxquotas stack-storage template

Stackable template with storage quotas definitions (ephemeral and pvc)

## Usage

```bash
sxquotas create stack-storage-quotas stack-storage
```

## Example output

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-storage-quotas
spec:
  hard:
    requests.storage: "5Gi"
    persistentvolumeclaims: "5"
    ephemeral-storage: "5Gi"
    thin.storageclass.storage.k8s.io/requests.storage: "0Gi"
    thin.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
    thin-csi.storageclass.storage.k8s.io/requests.storage: "0Gi"
    thin-csi.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
```