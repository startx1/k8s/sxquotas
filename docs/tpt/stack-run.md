# sxquotas stack-run template

Stackable template with deployment quotas definitions (deployment, ss, pod, job, ds)

## Usage

```bash
sxquotas create stack-run-quotas stack-run
```

## Example output

```yaml
apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-run-quotas
spec:
  hard:
    count/daemonsets.apps: "1"
    count/deployments.apps: "2"
    count/replicasets.apps: "6"
    replicationcontrollers: "0"
    pods: "10"
```