# sxquotas templates

## Basic templates

- [Default quota](default.md)
- [Kubernetes quota](k8s.md)
- [Openshift quota](openshift.md)
- [Full quota](full.md)
- [SXCollector quota](sxcollector.md)

## Stackable templates

- [Minimal quota](stack-minimal.md)
- [Basic add-on quota (cm, secret)](stack-basic.md)
- [Run add-on quota (deployment, ss, pod, job, ds)](stack-run.md)
- [Storage add-on quota (ephemeral and pvc)](stack-storage.md)
- [Network add-on quota (svc, ingress)](stack-network.md)
- [Openshift add-on quota (bc, imagestream)](stack-openshift.md)