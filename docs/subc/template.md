# sxquotas template

Template sub-command allow you to get informations about a given
resourceQuota template provided by sxquotas

## Usage

```bash
sxquotas template NAME [OPTIONS]...
```

## Arguments

| NAME | Mandatory | Description              |
| ---- | --------- | ------------------------ |
| NAME | Yes       | The name of the template |

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

Get information about the default template.

```bash
sxquotas template default
```
