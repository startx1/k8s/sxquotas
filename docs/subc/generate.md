# sxquotas generate

generate sub-command allow you to generate a new resourceQuota
resourceQuota based on the given template. If template is found, it
will display the resource quota named and without namespace definition.
Ready to be piped into a kubectl apply command.

## Usage

```bash
sxquotas generate NAME [TEMPLATE] [OPTIONS]...
```

## Arguments

| NAME     | Mandatory | Description                                                     |
| -------- | --------- | --------------------------------------------------------------- |
| NAME     | Yes       | The name of the quotas                                          |
| TEMPLATE | No        | The name of the template (use default template if not provided) |

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

### Display the example-quota resourceQuota based on the default template.

```bash
sxquotas generate example-quota
```

### Display the ocp-quota resourceQuota based on the openshift template.

```bash
sxquotas generate ocp-quota openshift
```

### Generate and manualy apply the k8s-quota based on the k8s template.

```bash
sxquotas generate k8s-quota k8s | kubectl apply -f -
```