# sxquotas create

create sub-command allow you to create a new resourceQuota
resourceQuota based on the given template. If quotas already exist, 
it will return an error 

## Usage

```bash
sxquotas create NAME [TEMPLATE] [OPTIONS]...
```

## Arguments

| NAME     | Mandatory | Description                                                     |
| -------- | --------- | --------------------------------------------------------------- |
| NAME     | Yes       | The name of the quotas                                          |
| TEMPLATE | No        | The name of the template (use default template if not provided) |

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

### Create the example-quota resourceQuota based on the default template.

```bash
sxquotas create example-quota
```

### Create the ocp-quota resourceQuota based on the openshift template.

```bash
sxquotas create ocp-quota openshift
```
