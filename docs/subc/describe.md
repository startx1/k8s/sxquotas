# sxquotas describe

describe sub-command allow you to list all resourceQuota present in
one or many namespaces and display it in a terminal display. 
You can choose bettween several format option that allow you to aggregate
information by namespace, resource or resourceQuota.

## Usage

```bash
sxquotas describe [NS_PATTERN] [OPTIONS]...
```

## Arguments

| NAME      | Mandatory | Description                                                                                                                |
| --------- | --------- | -------------------------------------------------------------------------------------------------------------------------- |
| NS_PATTERN  | No        | A regex to used for multiple namespace selection. (optional, use SXQUOTAS_NS env var or current namespace if not provided) |

## Specific options

| Flag                  | Description                                                                                                                                   |
| --------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| --format FORMAT       | Define the output format. could be 'detail', 'namespace' or 'resource' (default is detail)                                                    |
| --kubeconfig FILEPATH | Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided) |


## Environment variables

| Env name    | Description                                                   |
| ----------- | ------------------------------------------------------------- |
| SXQUOTAS_NS | Define the namespace to use instead of the current namespace. |


## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

### Describe quotas from the current namespace.

```bash
sxquotas describe
```

```bash
Namespace  ResourceQuotaName  ResourceName     Hard          Used       %      
---------- ------------------ ---------------- ------------- ---------- ------ 
sx1        test1              configmaps       13            8          61.54  
sx1        test1              limits.cpu       5250          250        4.76   
sx1        test1              limits.memory    139460608     134217728  96.24  
sx1        test1              requests.cpu     5100          100        1.96   
sx1        test1              requests.memory  72351744      67108864   92.75  
sx1        test1              requests.storage 7516192768    2147483648 28.57  
sx1        test2              requests.cpu     30            100        333.33 
sx1        test2              requests.memory  322122547200  67108864   0.02   
sx1        test2              configmaps       50            8          16.00  
sx1        test2              limits.memory    6442450944    134217728  2.08   
sx1        test2              nvidia.com/gpu   1             0          0.00 
```

### Describe synthetis of all resources in all openshift-* namespaces

```bash
sxquotas describe openshift-* --format resource
```

```bash
ResourceName                                                                    Hard         Used         %         
------------------------------------------------------------------------------- ------------ ------------ --------- 
ephemeral-storage                                                               32212254720  18408800256  28.58     
aws-slow-retain.storageclass.storage.k8s.io/requests.storage                    0            0            0.00      
services.nodeports                                                              0            0            0.00      
configmaps                                                                      50           8            8.00      
aws-generic-delete.storageclass.storage.k8s.io/requests.storage                 0            0            0.00      
openshift-storage.noobaa.io.storageclass.storage.k8s.io/persistentvolumeclaims  0            0            0.00      
odf-generic-retain.storageclass.storage.k8s.io/requests.storage                 0            0            0.00      
limits.memory                                                                   25769803776  10468982784  30.48     
odf-fs-retain.storageclass.storage.k8s.io/requests.storage                      0            0            0.00      
odf-storagecluster-ceph-rbd.storageclass.storage.k8s.io/persistentvolumeclaims  0            0            0.00      
aws-slow-retain.storageclass.storage.k8s.io/persistentvolumeclaims              0            0            0.00      
gp3.storageclass.storage.k8s.io/persistentvolumeclaims                          0            0            0.00      
aws-generic-delete.storageclass.storage.k8s.io/persistentvolumeclaims           0            0            0.00      
pods                                                                            20           8            30.00     
requests.memory                                                                 12884901888  5435817984   21.10     
gp3-csi.storageclass.storage.k8s.io/persistentvolumeclaims                      0            0            0.00      
odf-fs-delete.storageclass.storage.k8s.io/persistentvolumeclaims                0            0            0.00      
aws-fast-retain.storageclass.storage.k8s.io/requests.storage                    0            0            0.00      
gp3.storageclass.storage.k8s.io/requests.storage                                0            0            0.00      
openshift-storage.noobaa.io.storageclass.storage.k8s.io/requests.storage        0            0            0.00      
requests.storage                                                                0            0            0.00      
requests.cpu                                                                    6            2850         23750.00  
secrets                                                                         75           32           21.34     
persistentvolumeclaims                                                          0            0            0.00      
aws-slow-delete.storageclass.storage.k8s.io/persistentvolumeclaims              0            0            0.00      
count/deployments.apps                                                          15           7            35.01     
aws-generic-retain.storageclass.storage.k8s.io/requests.storage                 0            0            0.00      
aws-fast-delete.storageclass.storage.k8s.io/persistentvolumeclaims              0            0            0.00      
services.loadbalancers                                                          0            0            0.00      
aws-generic-retain.storageclass.storage.k8s.io/persistentvolumeclaims           0            0            0.00      
gp3-csi.storageclass.storage.k8s.io/requests.storage                            0            0            0.00      
aws-fast-delete.storageclass.storage.k8s.io/requests.storage                    0            0            0.00      
aws-slow-delete.storageclass.storage.k8s.io/requests.storage                    0            0            0.00      
odf-fs-delete.storageclass.storage.k8s.io/requests.storage                      0            0            0.00      
odf-generic-delete.storageclass.storage.k8s.io/persistentvolumeclaims           0            0            0.00      
aws-fast-retain.storageclass.storage.k8s.io/persistentvolumeclaims              0            0            0.00      
gp2.storageclass.storage.k8s.io/requests.storage                                0            0            0.00      
odf-fs-retain.storageclass.storage.k8s.io/persistentvolumeclaims                0            0            0.00      
odf-generic-retain.storageclass.storage.k8s.io/persistentvolumeclaims           0            0            0.00      
openshift.io/imagestreams                                                       0            0            0.00      
count/daemonsets.apps                                                           0            0            0.00      
gp2.storageclass.storage.k8s.io/persistentvolumeclaims                          0            0            0.00      
services                                                                        20           9            22.50     
odf-storagecluster-ceph-rbd.storageclass.storage.k8s.io/requests.storage        0            0            0.00      
replicationcontrollers                                                          5            0            0.00      
count/replicasets.apps                                                          40           11           13.76     
limits.cpu                                                                      12           8600         53750.01  
odf-generic-delete.storageclass.storage.k8s.io/requests.storage                 0            0            0.00  
```


### Describe synthetis of all resources by namespace in all openshift-* namespaces

```bash
sxquotas describe openshift-* --format namespace
```

```bash
Namespace               ResourceName                                                                    Hard         Used        %         
----------------------- ------------------------------------------------------------------------------- ------------ ----------- --------- 
openshift-gitops        pods                                                                            20           8           40.00     
openshift-gitops        requests.storage                                                                0            0           0.00      
openshift-gitops        openshift.io/imagestreams                                                       0            0           0.00      
openshift-gitops        services.loadbalancers                                                          0            0           0.00      
openshift-gitops        aws-generic-delete.storageclass.storage.k8s.io/requests.storage                 0            0           0.00      
openshift-gitops        odf-storagecluster-ceph-rbd.storageclass.storage.k8s.io/persistentvolumeclaims  0            0           0.00      
openshift-gitops        odf-generic-delete.storageclass.storage.k8s.io/requests.storage                 0            0           0.00      
openshift-gitops        aws-slow-retain.storageclass.storage.k8s.io/requests.storage                    0            0           0.00      
openshift-gitops        count/replicasets.apps                                                          40           11          27.50     
openshift-gitops        gp2.storageclass.storage.k8s.io/requests.storage                                0            0           0.00      
openshift-gitops        replicationcontrollers                                                          5            0           0.00      
openshift-gitops        aws-generic-retain.storageclass.storage.k8s.io/requests.storage                 0            0           0.00      
openshift-gitops        aws-fast-delete.storageclass.storage.k8s.io/persistentvolumeclaims              0            0           0.00      
openshift-gitops        aws-generic-retain.storageclass.storage.k8s.io/persistentvolumeclaims           0            0           0.00      
openshift-gitops        gp3.storageclass.storage.k8s.io/persistentvolumeclaims                          0            0           0.00      
openshift-gitops        odf-fs-retain.storageclass.storage.k8s.io/requests.storage                      0            0           0.00      
openshift-gitops        services.nodeports                                                              0            0           0.00      
openshift-gitops        odf-storagecluster-ceph-rbd.storageclass.storage.k8s.io/requests.storage        0            0           0.00      
openshift-gitops        odf-generic-retain.storageclass.storage.k8s.io/requests.storage                 0            0           0.00      
openshift-gitops        gp3-csi.storageclass.storage.k8s.io/persistentvolumeclaims                      0            0           0.00      
openshift-gitops        persistentvolumeclaims                                                          0            0           0.00      
openshift-gitops        aws-slow-delete.storageclass.storage.k8s.io/requests.storage                    0            0           0.00      
openshift-gitops        openshift-storage.noobaa.io.storageclass.storage.k8s.io/persistentvolumeclaims  0            0           0.00      
openshift-gitops        aws-slow-retain.storageclass.storage.k8s.io/persistentvolumeclaims              0            0           0.00      
openshift-gitops        configmaps                                                                      50           8           16.00     
openshift-gitops        odf-generic-retain.storageclass.storage.k8s.io/persistentvolumeclaims           0            0           0.00      
openshift-gitops        requests.cpu                                                                    6000         2850        47.50  
openshift-gitops        aws-fast-retain.storageclass.storage.k8s.io/persistentvolumeclaims              0            0           0.00      
openshift-gitops        aws-slow-delete.storageclass.storage.k8s.io/persistentvolumeclaims              0            0           0.00      
openshift-gitops        gp3.storageclass.storage.k8s.io/requests.storage                                0            0           0.00      
openshift-gitops        openshift-storage.noobaa.io.storageclass.storage.k8s.io/requests.storage        0            0           0.00      
openshift-gitops        gp2.storageclass.storage.k8s.io/persistentvolumeclaims                          0            0           0.00      
openshift-gitops        services                                                                        20           9           45.00     
openshift-gitops        odf-generic-delete.storageclass.storage.k8s.io/persistentvolumeclaims           0            0           0.00      
openshift-gitops        odf-fs-delete.storageclass.storage.k8s.io/persistentvolumeclaims                0            0           0.00      
openshift-gitops        aws-fast-retain.storageclass.storage.k8s.io/requests.storage                    0            0           0.00      
openshift-gitops        limits.cpu                                                                      12           8600        71666.67  
openshift-gitops        gp3-csi.storageclass.storage.k8s.io/requests.storage                            0            0           0.00      
openshift-gitops        requests.memory                                                                 12884901888  5435817984  42.19     
openshift-gitops        count/daemonsets.apps                                                           0            0           0.00      
openshift-gitops        odf-fs-delete.storageclass.storage.k8s.io/requests.storage                      0            0           0.00      
openshift-gitops        secrets                                                                         75           32          42.67     
openshift-gitops        aws-fast-delete.storageclass.storage.k8s.io/requests.storage                    0            0           0.00      
openshift-gitops        count/deployments.apps                                                          15           7           46.67     
openshift-gitops        limits.memory                                                                   25769803776  10468982784 40.62     
openshift-gitops        odf-fs-retain.storageclass.storage.k8s.io/persistentvolumeclaims                0            0           0.00      
openshift-gitops        aws-generic-delete.storageclass.storage.k8s.io/persistentvolumeclaims           0            0           0.00      
openshift-gitops        ephemeral-storage                                                               32212254720  18408800256 57.15     
openshift-host-network  limits.cpu                                                                      0            0           0.00      
openshift-host-network  limits.memory                                                                   0            0           0.00      
openshift-host-network  pods                                                                            0            0           0.00      
openshift-host-network  count/daemonsets.apps                                                           0            0           0.00      
openshift-host-network  count/deployments.apps                                                          0            0           0.00  
```
