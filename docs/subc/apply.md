# sxquotas apply

apply sub-command allow you to apply a template into an existing
resourceQuota. If quotas doesn't exist, it will return an error 

## Usage

```bash
sxquotas apply NAME [TEMPLATE] [OPTIONS]...
```

## Arguments

| NAME     | Mandatory | Description                                                     |
| -------- | --------- | --------------------------------------------------------------- |
| NAME     | Yes       | The name of the quotas                                          |
| TEMPLATE | No        | The name of the template (use default template if not provided) |

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

### Create and improve a stack

Create the example-quota resourceQuota based on the stack-minimal template and add network and storage quota to it

```bash
echo "---create the content of example-quota"
sxquotas create example-quota stack-minimal
echo "---display the content of example-quota"
kubectl describe resourcequota example-quota
echo "---add the storage content to it"
sxquotas apply example-quota stack-storage
echo "---display the content of example-quota"
kubectl describe resourcequota example-quota
echo "---add the network content to it"
sxquotas apply example-quota stack-network
echo "---display the content of example-quota"
kubectl describe resourcequota example-quota
```

### apply the ocp-quota resourceQuota based on the openshift template.

Create the example2-quota resourceQuota based on the default template and add network to it

```bash
echo "---create the content of example2-quota"
sxquotas create example2-quota stack-minimal && \ 
sxquotas apply  example2-quota stack-network
echo "---display the content of example2-quota"
kubectl describe resourcequota example2-quota
```
