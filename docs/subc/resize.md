# sxquotas resize

resize sub-command allow you to resize an existing resourceQuota
resourceQuota based on the given template

## Usage

```bash
sxquotas resize NAME [Increment] [OPTIONS]...
```

## Arguments

| NAME      | Mandatory | Description                                                                                                           |
| --------- | --------- | --------------------------------------------------------------------------------------------------------------------- |
| NAME      | Yes       | The name of the quotas                                                                                                |
| Increment | No        | The value of the increment. it could be a positive or negative integer or percentage. If not provided, 1 will be used |

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

### Resize the example-quota resourceQuota of 3. 

All Hard spec will be upgraded by 3.

```bash
sxquotas resize ocp-quota "3"
```

### Downsize the example-quota resourceQuota of -3. 

All Hard spec will be downgraded by 3. If negative will be set to zero.

```bash
sxquotas resize ocp-quota "-3"
```

### Resize the example-quota resourceQuota of 33%

```bash
sxquotas resize example-quota "33%"
```
