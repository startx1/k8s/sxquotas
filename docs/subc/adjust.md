# sxquotas adjust

adjust sub-command allow you to adjust an existing resourceQuota
resourceQuota based on the status.used informations

## Usage

```bash
sxquotas adjust NAME [Increment] [OPTIONS]...
```

## Arguments

| NAME      | Mandatory | Description                                                                                                                                                                                             |
| --------- | --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| NAME      | Yes       | The name of the quotas                                                                                                                                                                                  |
| Increment | No        | The value of the increment. It could be a positive or negative integer or percentage. If not provided, use 0 and adjusting will be done based only on the status.used informations of the resourceQuota |

## Generic options

| Flag    | Description                                                    |
| ------- | -------------------------------------------------------------- |
| --debug | Activates debug mode for detailed troubleshooting information. |
| --help  | Displays this help message and exits.                          |

The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.

## Examples

### Adjust the example-quota resourceQuota of 3. 

All spec.hard will be upgraded with the status.used incremented by 3.

```bash
sxquotas adjust ocp-quota "3"
```

### Downsize the example-quota resourceQuota of -3.

All spec.hard will be upgraded with the status.used decremented by 3.
If negative will be set to zero.

```bash
sxquotas adjust ocp-quota "-3"
```

### Adjust the example-quota resourceQuota of 33%

All spec.hard will be upgraded with the status.used augmented by 33% and 
rounded to the uper value.

```bash
sxquotas adjust example-quota "33%"
```
