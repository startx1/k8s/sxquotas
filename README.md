# sxquotas [![release](https://img.shields.io/badge/release-v0.2.1-blue.svg)](https://gitlab.com/startx1/k8s/sxquotas/-/releases/v0.2.1) [![last commit](https://img.shields.io/gitlab/last-commit/startx1/k8s/sxquotas.svg)](https://gitlab.com/startx1/k8s/sxquotas) [![Doc](https://readthedocs.org/projects/sxquotas/badge)](https://sxquotas.readthedocs.io) 

This project is focused on producing a `sxquotas` command line who can interact with a kubernetes cluster
and allow operations on the ResourceQuotas resources.

## Getting started

- [Install the `sxquotas` binary](https://sxquotas.readthedocs.io/en/devel/installation/) [(download)](https://gitlab.com/startx1/k8s/sxquotas/-/raw/stable/bin/sxquotas)
- Test it with `sxquotas version`
- Log into a kubernetes cluster `kubectl login ...`
- Create a new quotas based on the default template `sxquotas create myquotas`
- Get your quotas to see the changes `kubectl get resourcequotas myquotas -o yaml`
- Resize the quotas definitions `sxquotas resize myquotas 3`
- Get your quotas to see the changes `kubectl get resourcequotas myquotas -o yaml`
- Adjust the quotas definitions `sxquotas adjust myquotas`
- Get your quotas to see the changes `kubectl get resourcequotas myquotas -o yaml`
