package main

import (
	"os"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	sxUtilsQuotas "gitlab.com/startx1/k8s/sxquotas/pkg/utils"
)

const (
	ddSeqInit            = "Start the main function"
	ddSeqStart           = "Executing %s subcommand"
	ddCreateStart        = "Start creating the %s quota (based on %s template)"
	ddUpdateStart        = "Start updating the %s quota (based on %s template)"
	ddAdjustStart        = "Start adjusting the %s quota with %s"
	ddResizeStart        = "Start resizing the %s quota with %s"
	ddOkCreate           = "Quotas %s is added into namespace %s"
	ddOkApply            = "Quotas %s is updated into namespace %s"
	ddOkGenerate         = "Quotas %s is generated"
	ddOkResize           = "Quotas %s is resized into namespace %s"
	ddOkAdjust           = "Quotas %s is adjusted into namespace %s"
	errNoSubcmd          = "Expected 'templates', 'template', 'create', 'adjust', 'resize' or 'export' subcommands. See : sxquotas help."
	errTemplateReq       = "Template name is required for 'template' command"
	errQuotasNameReq     = "Quota name is required for '%s' command"
	errCreateGetTpl      = "Error accessing the template : %v"
	errUpdateGetTpl      = "Error accessing the template : %v"
	errResizeReq         = "resourceQuota name is required for '%s' command"
	errResizeLoad        = "Error loading resourceQuota %s in namespace %s : %v"
	errResizeResize      = "Error resizing resourceQuota %s in namespace %s : %v"
	errResizeApply       = "Error applying resourceQuota %s in namespace %s : %v"
	errAdjustReq         = "Quota name is required for 'adjust' command"
	errAdjustLoad        = "Error loading resourceQuota %s in namespace %s : %v"
	errAdjustAdjust      = "Error adjusting resourceQuota %s in namespace %s : %v"
	errAdjustApply       = "Error applying resourceQuota %s in namespace %s : %v"
	errApplyLoad         = "Error loading resourceQuota %s in namespace %s : %v"
	errApplyMerge        = "Error resizing resourceQuota %s in namespace %s : %v"
	errExportDo          = "Error exporting resourceQuota %s in namespace %s : %v"
	errApplyApply        = "Error applying resourceQuota %s in namespace %s : %v"
	errCreateGenerate    = "Error generating resourceQuota %s : %v"
	errApply             = "Error applying resourceQuota %s into the namespace %s : %v"
	errCreate            = "Error creating resourceQuota %s into the namespace %s : %v"
	errUpdateTemplateReq = "Template name is required for 'update' command"
)

func main() {
	display := sxUtils.NewCmdDisplay("main")
	display.Debug(ddSeqInit)
	argsQuotas := sxUtilsQuotas.NewSXQuotasArgParser(os.Args)
	argsQuotas.Prepare()
	argsQuotas.Exec()
}
