package templates

import (
	"errors"
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

// TemplateStack represents a stack of Template objects
type TemplateStack struct {
	list    []*Template
	display *sxUtils.CmdDisplay
}

// AddTemplate adds a template to the stack
func (ts *TemplateStack) AddTemplate(template *Template) {
	ts.display.Debug("Adding template " + template.name + " into the template stack")
	ts.list = append(ts.list, template)
}

// displayTemplate show the template stack
func (ts *TemplateStack) DisplayTemplates() {
	ts.display.Debug("Display the template stack")
	fmt.Printf("%-15s %-7s %s\n", "Name", "Enabled", "Description")
	for _, template := range ts.list {
		fmt.Printf("%-15s %-7v %s\n", template.name, template.enabled, template.desc)
	}
}

// displayTemplate show the template stack
func (ts *TemplateStack) DisplayTemplateInfo(name string) {
	ts.display.Debug("Display template " + name + " informations from the templates stack")
	template, err := ts.findByName(name)
	if err != nil {
		ts.display.ExitError(err.Error(), 20)
	}
	template.GetInfo()
}

// displayTemplate show the template stack
func (ts *TemplateStack) DisplayTemplate(name string) {
	ts.display.Debug("Display template " + name + " content from the templates stack")
	template, err := ts.findByName(name)
	if err != nil {
		ts.display.ExitError(err.Error(), 20)
	}
	template.GetContent()
}

// displayTemplate show the template stack
func (ts *TemplateStack) GetTemplate(name string) (*Template, error) {
	ts.display.Debug("Get " + name + " from the template stack")
	return ts.findByName(name)
}

// UpdateTemplate updates an existing template in the stack
func (ts *TemplateStack) UpdateTemplate(name string, updatedTemplate *Template) error {
	ts.display.Debug("Update " + name + " from the template stack")
	for i, template := range ts.list {
		if template.name == name {
			ts.list[i] = updatedTemplate
			return nil
		}
	}
	return errors.New("template not found")
}

// RemoveTemplate removes a template from the stack
func (ts *TemplateStack) RemoveTemplate(name string) error {
	ts.display.Debug("Remove " + name + " from the template stack")
	for i, template := range ts.list {
		if template.name == name {
			ts.list = append(ts.list[:i], ts.list[i+1:]...)
			return nil
		}
	}
	return errors.New("template not found")
}

// Find a template by its name from the stack
func (ts *TemplateStack) findByName(name string) (*Template, error) {
	for _, template := range ts.list {
		if template.name == name {
			return template, nil
		}
	}
	return nil, errors.New("template " + name + " not found")
}

// NewTemplateStack creates a new instance of TemplateStack
func NewTemplateStack() *TemplateStack {
	stack := &TemplateStack{}
	stack.display = sxUtils.NewCmdDisplay(GroupName)
	stack.display.Debug("Init the template stack")
	stack.AddTemplate(NewTemplate(
		"default",
		"Default template with basic quotas definitions",
		`apiVersion: v1
kind: ResourceQuota
metadata:
  name: default-quotas
spec:
  hard:
    limits.cpu: "1500m"
    limits.memory: "2Gi"
    limits.ephemeral-storage: "2Gi"
    requests.cpu: "750m"
    requests.memory: "1Gi"
    requests.ephemeral-storage: "1Gi"
    ephemeral-storage: "4Gi"
    count/secrets: "20"
    count/configmaps: "5"
    count/replicationcontrollers: "0"
    count/deployments.apps: "2"
    count/replicasets.apps: "4"
    count/statefulsets.apps: "1"
    count/daemonsets.apps: "1"
    count/jobs.batch: "2"
    count/cronjobs.batch: "1"
    count/services: "1"
    services: "1"
    services.loadbalancers: "0"
    services.nodeports: "0"
    pods: "4"`,
		true))
	stack.AddTemplate(NewTemplate(
		"openshift",
		"Template designed for a basic openshift install",
		`apiVersion: v1
kind: ResourceQuota
metadata:
  name: openshift-quotas
spec:
  hard:
    limits.cpu: "1500m"
    limits.memory: "2Gi"
    limits.ephemeral-storage: "2Gi"
    requests.cpu: "750m"
    requests.memory: "1Gi"
    requests.ephemeral-storage: "1Gi"
    requests.storage: "1Gi"
    ephemeral-storage: "4Gi"
    thin.storageclass.storage.k8s.io/requests.storage: "0Gi"
    thin.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
    thin-csi.storageclass.storage.k8s.io/requests.storage: "0Gi"
    thin-csi.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
    myexamplestorageclass.storageclass.storage.k8s.io/requests.storage: "0Gi"
    myexamplestorageclass.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
    persistentvolumeclaims: "2"
    count/persistentvolumeclaims: "2"
    count/secrets: "20"
    count/configmaps: "5"
    count/replicationcontrollers: "0"
    count/deployments.apps: "2"
    count/replicasets.apps: "4"
    count/statefulsets.apps: "1"
    count/daemonsets.apps: "1"
    count/jobs.batch: "2"
    count/cronjobs.batch: "1"
    count/services: "1"
    services: "1"
    services.loadbalancers: "0"
    services.nodeports: "0"
    openshift.io/imagestreams: "2"
    pods: "4"`,
		true))
	stack.AddTemplate(NewTemplate(
		"k8s",
		"Template designed for a basic kubernetes install",
		`apiVersion: v1
kind: ResourceQuota
metadata:
  name: k8s-quotas
spec:
  hard:
    limits.cpu: "1500m"
    limits.memory: "2Gi"
    limits.ephemeral-storage: "2Gi"
    requests.cpu: "750m"
    requests.memory: "1Gi"
    requests.ephemeral-storage: "1Gi"
    requests.storage: "1Gi"
    ephemeral-storage: "4Gi"
    persistentvolumeclaims: "2"
    count/persistentvolumeclaims: "2"
    count/secrets: "20"
    count/configmaps: "5"
    count/deployments.apps: "2"
    count/replicasets.apps: "4"
    count/statefulsets.apps: "1"
    count/daemonsets.apps: "1"
    count/jobs.batch: "2"
    count/cronjobs.batch: "1"
    count/services: "1"
    services: "1"
    services.loadbalancers: "0"
    services.nodeports: "0"
    pods: "4"`,
		true))
	stack.AddTemplate(NewTemplate(
		"full",
		"Template designed for a full resourceQuota definition",
		`apiVersion: v1
kind: ResourceQuota
metadata:
  name: full-quotas
spec:
  hard:
    limits.cpu: "1500m"
    limits.memory: "2Gi"
    limits.ephemeral-storage: "2Gi"
    requests.cpu: "750m"
    requests.memory: "1Gi"
    requests.ephemeral-storage: "1Gi"
    requests.storage: "1Gi"
    ephemeral-storage: "4Gi"
    myexamplestorageclass.storageclass.storage.k8s.io/requests.storage: "0Gi"
    myexamplestorageclass.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
    persistentvolumeclaims: "2"
    count/persistentvolumeclaims: "2"
    count/secrets: "20"
    count/configmaps: "5"
    count/deployments.apps: "2"
    count/replicasets.apps: "4"
    count/statefulsets.apps: "1"
    count/daemonsets.apps: "1"
    count/jobs.batch: "2"
    count/cronjobs.batch: "1"
    count/services: "1"
    services: "1"
    services.loadbalancers: "0"
    services.nodeports: "0"
    pods: "4"`,
		true))
	stack.AddTemplate(NewTemplate(
		"stack-minimal",
		"Stackable template with minimal quotas definitions (request & limits)",
		`apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-minimal-quotas
spec:
  hard:
    limits.cpu: "1500m"
    limits.memory: "2Gi"
    requests.cpu: "750m"
    requests.memory: "1Gi"
    pods: "4"`,
		true))
	stack.AddTemplate(NewTemplate(
		"stack-basic",
		"Stackable template with basic quotas definitions (cm, secret)",
		`apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-basic-quotas
spec:
  hard:
    count/secrets: "20"
    count/configmaps: "5"
    pods: "4"`,
		true))
	stack.AddTemplate(NewTemplate(
		"stack-network",
		"Stackable template with network quotas definitions (svc)",
		`apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-network-quotas
spec:
  hard:
    count/services: "1"
    services: "1"
    services.loadbalancers: "0"
    services.nodeports: "0"`,
		true))
	stack.AddTemplate(NewTemplate(
		"stack-storage",
		"Stackable template with storage quotas definitions (ephemeral and pvc)",
		`apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-storage-quotas
spec:
  hard:
    limits.ephemeral-storage: "2Gi"
    requests.ephemeral-storage: "1Gi"
    requests.storage: "1Gi"
    ephemeral-storage: "4Gi"
    thin.storageclass.storage.k8s.io/requests.storage: "0Gi"
    thin.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
    thin-csi.storageclass.storage.k8s.io/requests.storage: "0Gi"
    thin-csi.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
    myexamplestorageclass.storageclass.storage.k8s.io/requests.storage: "0Gi"
    myexamplestorageclass.storageclass.storage.k8s.io/persistentvolumeclaims: "0"
    persistentvolumeclaims: "2"
    count/persistentvolumeclaims: "2"`,
		true))
	stack.AddTemplate(NewTemplate(
		"stack-run",
		"Stackable template with deployment quotas definitions (deployment, ss, pod, job, ds)",
		`apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-run-quotas
spec:
  hard:
    count/replicationcontrollers: "0"
    count/deployments.apps: "2"
    count/replicasets.apps: "4"
    count/statefulsets.apps: "1"
    count/daemonsets.apps: "1"
    count/jobs.batch: "2"
    count/cronjobs.batch: "1"
    pods: "4"`,
		true))
	stack.AddTemplate(NewTemplate(
		"stack-openshift",
		"Stackable template with openshift quotas definitions (imagestream, build)",
		`apiVersion: v1
kind: ResourceQuota
metadata:
  name: stack-openshift-quotas
spec:
  hard:
    count/replicationcontrollers: "0"
    openshift.io/imagestreams: "2"`,
		true))
	stack.AddTemplate(NewTemplate(
		"sxcollector",
		"Template designed for a sxcollector resourceQuota definition",
		`apiVersion: v1
  kind: ResourceQuota
  metadata:
    name: full-quotas
  spec:
    hard:
      limits.cpu: "100000m"
      limits.memory: "10Ti"
      limits.ephemeral-storage: "10Ti"
      requests.cpu: "100000m"
      requests.memory: "10Ti"
      requests.ephemeral-storage: "10Ti"
      requests.storage: "10Ti"
      ephemeral-storage: "10Ti"
      persistentvolumeclaims: "1000"
      count/persistentvolumeclaims: "1000"
      count/secrets: "1000"
      count/configmaps: "1000"
      count/deployments.apps: "1000"
      count/replicasets.apps: "1000"
      count/statefulsets.apps: "1000"
      count/daemonsets.apps: "1000"
      count/jobs.batch: "1000"
      count/cronjobs.batch: "1000"
      count/services: "1000"
      services: "1000"
      services.loadbalancers: "1000"
      services.nodeports: "1000"
      pods: "1000"`,
		true))
	return stack
}
