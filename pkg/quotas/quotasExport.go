/*
Copyright 2021 Startx, member of LaHSC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package quotas

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"math"
	"strconv"
	"strings"

	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// GroupNameExport is the group name use in this package
	GroupNameExport                    = "quotas.export"
	ddInitRQExporter                   = "Init Exporting %s resourceQuotas"
	ddInitListNamespaceRQ              = "Init listing resourceQuota in namespace %s"
	ddInitExtractNamespaceRQ           = "Init extracting resourceQuota in namespace %s"
	errExportList                      = "Error listing ResourceQuotas in namespace %s : %s"
	errExportWriteCSV                  = "error writing CSV header : %v"
	errExportWriteCSVLine              = "error writing CSV header for %s : %v"
	errExportDo                        = "error exporting : %v"
	errExtractNamespaceRQ              = "error extracting namespace %s : %v"
	errRQExporterGetNS                 = "error getting the list of namespaces matching %s : %v"
	errRQExporterGetNSRQ               = "error getting resourceQuota in namespace %s : %v"
	ddRQExporterStart                  = "Get all resourceQuotas in namespaces matching %s pattern"
	ddRQExporterStartNS                = "Get all resourceQuotas in namespace %s"
	ddRQExporterParseRQ                = "%s - %s - Parsed Hard : %d, Used: %d"
	ddRQExporterFullParseRQR           = "%s - %s - %s - Return Hard : %s, Used: %s, Ratio: %s"
	ddRQExporterResourceParseRQR       = "%s - Return Hard : %s, Used: %s, Ratio: %s"
	ddRQExporterNamespaceParseRQR      = "%s - %s - Return Hard : %s, Used: %s, Ratio: %s"
	errRQExporterWriteCSV              = "error writing CSV header : %v"
	errRQExporterFullWriteCSVLine      = "error writing CSV line for resourceQuota %s in namespace %s - %s : %v"
	errRQExporterResourceWriteCSVLine  = "error writing CSV line for resourceQuota %s : %v"
	errRQExporterNamespaceWriteCSVLine = "error writing CSV line for resourceQuota %s in namespace %s : %v"
	warnPercentageOfDivisorZero        = "warning in namespace %s - resourceQuota %s - resourceName %s - error divisor is zero"
	ddRQExporterGenerateCSVFull        = "Start Generating full CSV report"
	ddInitRQDescriber                  = "Init Describing %s resourceQuotas"
	errRQDescriberGetNS                = "error getting the list of namespaces matching %s : %v"
	errRQDescriberGetNSRQ              = "error getting resourceQuota in namespace %s : %v"
	errRQDescribeGetExport             = "error exporting for describe : %v"
)

// RQExporter return the list of all resource, in all resourceQuotas for the namespaces matching the namespacePattern
func RQExporter(namespacePattern string, k8sclient *sxKCli.K8sClient, separatorNew string, hasHeader bool, outputFormat string) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(fmt.Sprintf(ddInitRQExporter, namespacePattern))
	var nsList []string
	var errListNS error
	separator := ','
	if separatorNew != "" {
		x := []rune(separatorNew)
		separator = x[0]
	}
	if namespacePattern == "" {
		nsList = append(nsList, k8sclient.GetCurrentNamespace())
	} else {
		nsList, errListNS = k8sclient.ListAllMatchingNamespaces(namespacePattern)
	}
	if errListNS != nil {
		display.Error(fmt.Sprintf(errRQExporterGetNS, namespacePattern, errListNS))
		return "", errListNS
	}
	display.Debug(fmt.Sprintf(ddRQExporterStart, namespacePattern))
	var results []map[string]string
	for _, namespace := range nsList {
		display.Debug(fmt.Sprintf(ddRQExporterStartNS, namespace))
		extractRQ, errExtractRQ := ExtractNamespaceRQ(namespace, k8sclient)
		if errExtractRQ != nil {
			display.Error(fmt.Sprintf(errRQExporterGetNSRQ, namespace, errExtractRQ))
		}
		for _, quota := range extractRQ {
			display.Debug(fmt.Sprintf(
				ddRQExporterParseRQ,
				quota.Name,
				quota.Namespace,
				len(quota.Spec.Hard),
				len(quota.Status.Used),
			))

			for resourceName, hard := range quota.Spec.Hard {
				used := quota.Status.Used[resourceName]
				quotaDetails := map[string]string{
					"Namespace":         quota.Namespace,
					"ResourceQuotaName": quota.Name,
					"ResourceName":      string(resourceName),
					"Hard":              hard.String(),
					"Used":              used.String(),
				}
				results = append(results, quotaDetails)
			}
		}
	}

	var csvContent string
	var errGen error
	switch outputFormat {
	case "namespace", "namespaces", "ns", "n":
		csvContent, errGen = RQExporterGenerateCSVNamespaces(separator, hasHeader, results)
	case "resource", "resources", "r":
		csvContent, errGen = RQExporterGenerateCSVResources(separator, hasHeader, results)
	default:
		csvContent, errGen = RQExporterGenerateCSVFull(separator, hasHeader, results)
	}

	if errGen != nil {
		return "", fmt.Errorf(errRQExporterWriteCSV, errGen)
	}

	return csvContent, nil
}

// RQExporter return the list of all resource, in all resourceQuotas for the namespaces matching the namespacePattern
func RQDescriber(namespacePattern string, k8sclient *sxKCli.K8sClient, outputFormat string) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(fmt.Sprintf(ddInitRQDescriber, namespacePattern))

	csvContent, errExport := RQExporter(namespacePattern, k8sclient, ",", true, outputFormat)
	if errExport != nil {
		display.Error(fmt.Sprintf(errRQDescribeGetExport, errExport))
		return "", errExport
	}

	// Create a new CSV reader
	reader := csv.NewReader(strings.NewReader(csvContent))

	// Read all records from the CSV
	records, err := reader.ReadAll()
	if err != nil {
		fmt.Println("Error:", err)
		return "", err
	}

	// Find maximum length for each column
	maxColLengths := make([]int, len(records[0]))
	for _, row := range records {
		for i, column := range row {
			if len(column) > maxColLengths[i] {
				maxColLengths[i] = len(column) + 1
			}
		}
	}

	// Display each line with fixed column lengths
	output := ""
	for j, row := range records {
		for i, column := range row {
			output = output + fmt.Sprintf("%-*s ", maxColLengths[i], column)
		}
		output = output + "\n"
		// Print separation line after the first header line
		if j == 0 {
			for _, length := range maxColLengths {
				output = output + strings.Repeat("-", length) + " "
			}
			output = output + "\n"
		}
	}

	return output, nil
}

// RQExporterGenerateCSVNamespaces return the CSV content for the given resultSet
func RQExporterGenerateCSVNamespaces(separator rune, hasHeader bool, resultSet []map[string]string) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(ddRQExporterGenerateCSVFull)

	// Create a buffer and CSV writer to write into the buffer
	var csvOutput bytes.Buffer
	writer := csv.NewWriter(&csvOutput)
	writer.Comma = separator
	// Write CSV headers
	if hasHeader {
		errWH := writer.Write([]string{"Namespace", "ResourceName", "Hard", "Used", "%"})
		if errWH != nil {
			return "", fmt.Errorf(errRQExporterWriteCSV, errWH)
		}
	}
	matrixHard := make(map[string]map[string]int)
	matrixUsed := make(map[string]map[string]int)
	matrixPercentage := make(map[string]map[string]float64)
	// Sum the result Hard (global) and Used (per namespace)
	for _, qt := range resultSet {
		hard := NewQuotasDef(qt["ResourceName"], qt["Hard"])
		used := NewQuotasDef(qt["ResourceName"], qt["Used"])
		if _, exists := matrixHard[qt["Namespace"]]; !exists {
			matrixHard[qt["Namespace"]] = make(map[string]int)
		}
		if _, exists := matrixHard[qt["Namespace"]][qt["ResourceName"]]; !exists {
			matrixHard[qt["Namespace"]][qt["ResourceName"]] = 0
		}
		if _, exists := matrixUsed[qt["Namespace"]]; !exists {
			matrixUsed[qt["Namespace"]] = make(map[string]int)
		}
		if _, exists := matrixUsed[qt["Namespace"]][qt["ResourceName"]]; !exists {
			matrixUsed[qt["Namespace"]][qt["ResourceName"]] = 0
		}
		if hard.valInt > matrixHard[qt["Namespace"]][qt["ResourceName"]] {
			matrixHard[qt["Namespace"]][qt["ResourceName"]] = hard.valInt
		}
		matrixUsed[qt["Namespace"]][qt["ResourceName"]] = used.valInt
	}
	// Sum the percentage per namespace
	for ns, ru := range matrixUsed {
		for rn, rv := range ru {
			if _, exists := matrixHard[ns]; !exists {
				matrixHard[ns] = make(map[string]int)
			}
			if _, exists := matrixHard[ns][rn]; !exists {
				matrixHard[ns][rn] = 0
			}
			if _, exists := matrixPercentage[ns]; !exists {
				matrixPercentage[ns] = make(map[string]float64)
			}
			percentage := percentageOf(
				ns,
				"none",
				rn,
				rv,
				matrixHard[ns][rn],
			)
			if _, exists := matrixPercentage[ns][rn]; !exists {
				matrixPercentage[ns][rn] = percentage
			} else {
				average := (matrixPercentage[ns][rn] + percentage) / 2
				roundedAverage := math.Ceil(average*100) / 100
				matrixPercentage[ns][rn] += roundedAverage
			}
		}
	}

	// return the CSV content
	for ns, rvu := range matrixUsed {
		for rn, rv := range rvu {
			display.Debug(fmt.Sprintf(
				ddRQExporterNamespaceParseRQR,
				ns,
				rn,
				strconv.Itoa(matrixHard[ns][rn]),
				strconv.Itoa(rv),
				fmt.Sprintf("%.2f", matrixPercentage[ns][rn]),
			))
			errWL := writer.Write([]string{
				ns,
				rn,
				strconv.Itoa(matrixHard[ns][rn]),
				strconv.Itoa(rv),
				fmt.Sprintf("%.2f", matrixPercentage[ns][rn]),
			})
			if errWL != nil {
				return "", fmt.Errorf(errRQExporterNamespaceWriteCSVLine,
					ns,
					rn,
					errWL,
				)
			}
		}
	}
	writer.Flush()

	return csvOutput.String(), nil
}

// RQExporterGenerateCSVResources return the CSV content for the given resultSet
func RQExporterGenerateCSVResources(separator rune, hasHeader bool, resultSet []map[string]string) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(ddRQExporterGenerateCSVFull)

	// Create a buffer and CSV writer to write into the buffer
	var csvOutput bytes.Buffer
	writer := csv.NewWriter(&csvOutput)
	writer.Comma = separator
	// Write CSV headers
	if hasHeader {
		errWH := writer.Write([]string{"ResourceName", "Hard", "Used", "%"})
		if errWH != nil {
			return "", fmt.Errorf(errRQExporterWriteCSV, errWH)
		}
	}
	matrixHard := make(map[string]map[string]int)
	matrixUsed := make(map[string]map[string]int)
	matrixPercentage := make(map[string]map[string]float64)
	// Sum the result Hard (global) and Used (per namespace)
	for _, qt := range resultSet {
		hard := NewQuotasDef(qt["ResourceName"], qt["Hard"])
		used := NewQuotasDef(qt["ResourceName"], qt["Used"])
		if _, exists := matrixHard[qt["Namespace"]]; !exists {
			matrixHard[qt["Namespace"]] = make(map[string]int)
		}
		if _, exists := matrixHard[qt["Namespace"]][qt["ResourceName"]]; !exists {
			matrixHard[qt["Namespace"]][qt["ResourceName"]] = 0
		}
		if _, exists := matrixUsed[qt["Namespace"]]; !exists {
			matrixUsed[qt["Namespace"]] = make(map[string]int)
		}
		if _, exists := matrixUsed[qt["Namespace"]][qt["ResourceName"]]; !exists {
			matrixUsed[qt["Namespace"]][qt["ResourceName"]] = 0
		}
		if hard.valInt > matrixHard[qt["Namespace"]][qt["ResourceName"]] {
			matrixHard[qt["Namespace"]][qt["ResourceName"]] = hard.valInt
		}
		matrixUsed[qt["Namespace"]][qt["ResourceName"]] = used.valInt
	}
	// Sum the percentage per namespace
	for ns, ru := range matrixUsed {
		for rn, rv := range ru {
			if _, exists := matrixHard[ns]; !exists {
				matrixHard[ns] = make(map[string]int)
			}
			if _, exists := matrixHard[ns][rn]; !exists {
				matrixHard[ns][rn] = 0
			}
			if _, exists := matrixPercentage[ns]; !exists {
				matrixPercentage[ns] = make(map[string]float64)
			}
			percentage := percentageOf(
				ns,
				"none",
				rn,
				rv,
				matrixHard[ns][rn],
			)
			if _, exists := matrixPercentage[ns][rn]; !exists {
				matrixPercentage[ns][rn] = percentage
			} else {
				average := (matrixPercentage[ns][rn] + percentage) / 2
				roundedAverage := math.Ceil(average*100) / 100
				matrixPercentage[ns][rn] += roundedAverage
			}
		}
	}
	// reduce by removing the namespace layer
	resultHard := make(map[string]int)
	resultUsed := make(map[string]int)
	resultPercentage := make(map[string]float64)
	for ns, ru := range matrixUsed {
		for rn, rv := range ru {
			if _, exists := resultHard[rn]; !exists {
				resultHard[rn] = 0
			}
			if _, exists := resultUsed[rn]; !exists {
				resultUsed[rn] = 0
			}
			if _, exists := resultPercentage[rn]; !exists {
				resultPercentage[rn] = float64(0)
			}
			resultHard[rn] += matrixHard[ns][rn]
			resultUsed[rn] += rv
			percentage := percentageOf(
				ns,
				"none",
				rn,
				rv,
				matrixHard[ns][rn],
			)
			if _, exists := resultPercentage[rn]; !exists {
				resultPercentage[rn] = percentage
			} else {
				average := (resultPercentage[rn] + percentage) / 2
				roundedAverage := math.Ceil(average*100) / 100
				resultPercentage[rn] += roundedAverage
			}
		}
	}

	// return the CSV content
	for rn, rv := range resultUsed {
		display.Debug(fmt.Sprintf(
			ddRQExporterResourceParseRQR,
			rn,
			strconv.Itoa(resultHard[rn]),
			strconv.Itoa(rv),
			fmt.Sprintf("%.2f", resultPercentage[rn]),
		))
		errWL := writer.Write([]string{
			rn,
			strconv.Itoa(resultHard[rn]),
			strconv.Itoa(rv),
			fmt.Sprintf("%.2f", resultPercentage[rn]),
		})
		if errWL != nil {
			return "", fmt.Errorf(errRQExporterResourceWriteCSVLine,
				rn,
				errWL,
			)
		}
	}
	writer.Flush()

	return csvOutput.String(), nil
}

// RQExporterGenerateCSVFull return the CSV content for the given resultSet
func RQExporterGenerateCSVFull(separator rune, hasHeader bool, resultSet []map[string]string) (string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(ddRQExporterGenerateCSVFull)

	// Create a buffer and CSV writer to write into the buffer
	var csvOutput bytes.Buffer
	writer := csv.NewWriter(&csvOutput)
	writer.Comma = separator
	// Write CSV headers
	if hasHeader {
		errWH := writer.Write([]string{"Namespace", "ResourceQuotaName", "ResourceName", "Hard", "Used", "%"})
		if errWH != nil {
			return "", fmt.Errorf(errRQExporterWriteCSV, errWH)
		}
	}

	for _, qt := range resultSet {
		hard := NewQuotasDef(qt["ResourceName"], qt["Hard"])
		used := NewQuotasDef(qt["ResourceName"], qt["Used"])
		hardVal := ""
		usedVal := ""
		percentVal := ""
		switch hard.GetValUnit() {
		case "m", "Ki", "Mi", "Gi", "Ti", "Pi":
			hardVal = strconv.Itoa(hard.valInt)
		default:
			hardVal = hard.GetValString()
		}
		switch used.GetValUnit() {
		case "m", "Ki", "Mi", "Gi", "Ti", "Pi":
			usedVal = strconv.Itoa(used.valInt)
		default:
			usedVal = used.GetValString()
		}
		percentVal = fmt.Sprintf(
			"%.2f",
			percentageOf(
				qt["Namespace"],
				qt["ResourceQuotaName"],
				qt["ResourceName"],
				used.valInt,
				hard.valInt,
			),
		)
		display.Debug(fmt.Sprintf(
			ddRQExporterFullParseRQR,
			qt["ResourceQuotaName"],
			qt["Namespace"],
			qt["ResourceName"],
			hardVal,
			usedVal,
			percentVal,
		))
		errWL := writer.Write([]string{
			qt["Namespace"],
			qt["ResourceQuotaName"],
			qt["ResourceName"],
			hardVal,
			usedVal,
			percentVal,
		})
		if errWL != nil {
			return "", fmt.Errorf(errRQExporterFullWriteCSVLine,
				qt["ResourceQuotaName"],
				qt["Namespace"],
				qt["ResourceName"],
				errWL,
			)
		}
	}
	writer.Flush()

	return csvOutput.String(), nil
}

// percentageOf calculates the percentage of dividend from divisor
func percentageOf(ns string, rqn string, rn string, dividend, divisor int) float64 {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	if divisor == 0 {
		display.Debug(fmt.Sprintf(warnPercentageOfDivisorZero,
			rqn,
			ns,
			rn,
		))
		return 0
	}
	result := (float64(dividend) / float64(divisor)) * 100
	return result
}

// ListNamespaceRQ return the list of all quotas in a namespace
func ListNamespaceRQ(namespace string, k8sclient *sxKCli.K8sClient) ([]string, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(fmt.Sprintf(ddInitListNamespaceRQ, namespace))
	quotas, err := k8sclient.
		Clientset.
		CoreV1().
		ResourceQuotas(namespace).
		List(
			context.TODO(),
			metav1.ListOptions{},
		)
	if err != nil {
		return nil, err
	}

	var quotaNames []string
	for _, quota := range quotas.Items {
		quotaNames = append(quotaNames, quota.Name)
	}
	return quotaNames, nil
}

// ExtractNamespaceRQ grab all resourceQuotas from a namespace and return a list of corev1.ResourceQuota
func ExtractNamespaceRQ(namespace string, k8sclient *sxKCli.K8sClient) ([]corev1.ResourceQuota, error) {
	display := sxUtils.NewCmdDisplay(GroupNameExport)
	display.Debug(fmt.Sprintf(ddInitExtractNamespaceRQ, namespace))
	// Get all ResourceQuotas in the namespace
	quotasList, errList := k8sclient.
		Clientset.
		CoreV1().
		ResourceQuotas(namespace).
		List(
			context.TODO(),
			metav1.ListOptions{},
		)
	if errList != nil {
		display.Error(fmt.Sprintf(errExtractNamespaceRQ, namespace, errList))
		return nil, errList
	}
	return quotasList.Items, nil
}

// // ExportNamespaceRQ export resourceQuota informations into a csv format
// func ExportNamespaceRQ(namespace string, k8sclient *sxKCli.K8sClient) (string, error) {
// 	display := sxUtils.NewCmdDisplay(GroupNameExport)
// 	display.Debug(ddInitExportNamespaceRQ)
// 	// Get all ResourceQuotas in the namespace
// 	quotasList, errList := k8sclient.
// 		Clientset.
// 		CoreV1().
// 		ResourceQuotas(namespace).
// 		List(
// 			context.TODO(),
// 			metav1.ListOptions{},
// 		)
// 	if errList != nil {
// 		display.ExitError(
// 			fmt.Sprintf(
// 				errExportList,
// 				namespace,
// 				errList,
// 			),
// 			30,
// 		)
// 	}
// 	// Maps to hold sums of resources
// 	totalHard := make(map[corev1.ResourceName]resource.Quantity)
// 	totalUsed := make(map[corev1.ResourceName]resource.Quantity)
// 	// Aggregate data across all quotas
// 	for _, quota := range quotasList.Items {
// 		addRQQuota2Total("Hard", totalHard, quota.Status.Hard)
// 		addRQQuota2Total("Used", totalUsed, quota.Status.Used)
// 	}
// 	// Create a buffer and CSV writer to write into the buffer
// 	var csvOutput bytes.Buffer
// 	writer := csv.NewWriter(&csvOutput)
// 	writer.Comma = ';'
// 	// Write CSV headers
// 	errWH := writer.Write([]string{"Resource", "Total Hard", "Total Used"})
// 	if errWH != nil {
// 		return "", fmt.Errorf(errExportWriteCSV, errWH)
// 	}
// 	// Process each resource type found in totalHard or totalUsed
// 	for resource := range totalHard {
// 		hard := totalHard[resource]
// 		used := totalUsed[resource]
// 		errWL := writer.Write([]string{string(resource), hard.String(), used.String()})
// 		if errWL != nil {
// 			return "", fmt.Errorf(errExportWriteCSVLine, string(resource), errWL)
// 		}
// 	}
// 	writer.Flush()
// 	return csvOutput.String(), nil
// }

// // addRQQuota2Total adds resources from one ResourceList into another ResourceList map
// func addRQQuota2Total(kind string, total map[corev1.ResourceName]resource.Quantity, rs corev1.ResourceList) {
// 	display := sxUtils.NewCmdDisplay(GroupNameExport)
// 	for k, v := range rs {
// 		valueDisplay := ""
// 		if existing, ok := total[k]; ok {
// 			newValue := existing.DeepCopy()
// 			switch k {
// 			case
// 				"requests.memory",
// 				"limits.memory",
// 				"requests.storage",
// 				"ephemeral-storage":
// 				nextMili := existing.MilliValue() + v.MilliValue()
// 				display.Debug(fmt.Sprintf("for %s : move from %d to %d (mem)", k.String(), existing.MilliValue(), nextMili))
// 				newValue.SetMilli(nextMili)
// 			case
// 				"requests.cpu",
// 				"limits.cpu":
// 				nextMili := existing.MilliValue() + v.MilliValue()
// 				display.Debug(fmt.Sprintf("for %s : move from %d to %d (cpu)", k.String(), existing.MilliValue(), nextMili))
// 				newValue.SetMilli(nextMili)
// 			default:
// 				nextVal := existing.Value() + v.Value()
// 				display.Debug(fmt.Sprintf("for %s : move from %d to %d (cpu)", k.String(), existing.Value(), nextVal))
// 				newValue.Set(nextVal)
// 			}
// 			total[k] = newValue
// 			valueDisplay = newValue.String()
// 		} else {
// 			valueDisplay = v.String()
// 			total[k] = v.DeepCopy()
// 		}
// 		display.Debug(fmt.Sprintf("existing %s %s value : %s", kind, k, valueDisplay))
// 	}
// }
