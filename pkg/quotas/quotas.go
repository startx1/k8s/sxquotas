/*
Copyright 2021 Startx, member of LaHSC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package quotas

import (
	"context"
	"fmt"

	sxIncrementor "gitlab.com/startx1/k8s/go-libs/pkg/incrementor"
	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	sxTpl "gitlab.com/startx1/k8s/sxquotas/pkg/templates"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/yaml"
)

const (
	// GroupAPI is the group API scope use in this package
	GroupAPI = "sxquotas.k8s.startx.fr"
	// GroupName is the group name use in this package
	GroupName                     = "quotas"
	ddInitNewRQ                   = "Init ResourceQuota object"
	ddInitnewRQ                   = "Init a new ResourceQuota object"
	ddInitLoad                    = "Load the resource quotas %s from the cluster"
	ddInitResize                  = "Resize the resource quotas %s to %s"
	ddInitAdjust                  = "Adjust the resource quotas %s to %s"
	ddInitMergeTpl                = "Merge %s template into the %s Quotas (obj)"
	ddInitApply                   = "Apply the resource quotas %s in namespace %s the cluster"
	ddInitDelete                  = "Delete the resource quotas %s in namespace %s the cluster"
	ddInitGetYaml                 = "Return yaml content of the resource quotas %s in namespace %s"
	ddInitPrint                   = "Print content of the resource quotas %s in namespace %s"
	ddInitincrementResourceValues = "Increments the resource quotas list"
	ddIncrementRV                 = "Increment from %s to %s for %s"
	ddMergeTplAddVal              = "Adding %s with value %s into the %s Quotas"
	ddDebugQtStart                = "DEBUG the resourceQuota"
	ddDebugQtQuotas               = "DEBUG - resourceQuota resource UUID         : %s"
	ddDebugQtValName              = "DEBUG - resourceQuota name                  : %s"
	ddDebugQtValNamespace         = "DEBUG - resourceQuota namespace             : %s"
	ddDebugQtValIsLoaded          = "DEBUG - resourceQuota is loaded             : %t"
	ddDebugQtValQuotaRule         = "DEBUG - resourceQuota define a quota for %s : %s"
	warnErrorIncrement            = "quota %s with value %s could not increment with %s : %v"
	errLoadErr                    = "resource quota '%s' in namespace '%s' has an error : %v"
	errLoadFound                  = "resource quota '%s' in namespace '%s' is not found : %v"
	errMergeTplFound              = "error getting template : %v"
	errMergeTplFoundQ             = "failed to get destQuota: %s in %s : %v"
	errApplyExec                  = "failed to apply ResourceQuota '%s' in namespace '%s' update to the cluster : %v"
	errDeleteExec                 = "failed to delete ResourceQuota '%s' in namespace '%s' delete to the cluster : %v"
)

//
// Definitions of the ResourceQuotas object
//

// ResourceQuotas represent the object properties
type ResourceQuotas struct {
	k8sclient *sxKCli.K8sClient
	Quotas    *corev1.ResourceQuota
	Name      string
	Namespace string
	IsLoaded  bool
	display   *sxUtils.CmdDisplay
}

// NewRQs create a new ResourceQuotas ready to use the clientset
// to interact with the kubernetes cluster
func NewRQ(name string, namespace string, k8sclient *sxKCli.K8sClient) *ResourceQuotas {
	// sxUtils.DisplayDebug(GroupName,ddInitNewRQ)
	resourceQuota := newRQ(name, namespace)
	return &ResourceQuotas{
		k8sclient: k8sclient,
		Name:      name,
		Namespace: namespace,
		Quotas:    resourceQuota,
		IsLoaded:  false,
		display:   sxUtils.NewCmdDisplay(GroupNameDef),
	}
}

// newRQ create a new ResourceQuotaSpec with name and namespace set
// to the given parameters. everitying else is default
func newRQ(name string, namespace string) *corev1.ResourceQuota {
	// display := sxUtils.NewCmdDisplay(GroupName)
	// display.Debug(ddInitnewRQ)
	// Define the ResourceQuota spec.
	resourceQuotaSpec := corev1.ResourceQuotaSpec{
		Hard: corev1.ResourceList{
			corev1.ResourcePods:           resource.MustParse("10"),
			corev1.ResourceServices:       resource.MustParse("5"),
			corev1.ResourceRequestsCPU:    resource.MustParse("1"),
			corev1.ResourceRequestsMemory: resource.MustParse("1Gi"),
			corev1.ResourceLimitsCPU:      resource.MustParse("2"),
			corev1.ResourceLimitsMemory:   resource.MustParse("2Gi"),
		},
	}
	// Define the ResourceQuota spec.
	resourceQuota := &corev1.ResourceQuota{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: resourceQuotaSpec,
	}
	return resourceQuota
}

// Load load the resource quotas %s from the cluster
// or return an error
func (rq *ResourceQuotas) Load() error {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug(fmt.Sprintf(ddInitLoad, rq.Name))
	quotas, err := rq.k8sclient.Clientset.CoreV1().ResourceQuotas(rq.Namespace).Get(
		context.TODO(),
		rq.Name,
		metav1.GetOptions{},
	)
	if err != nil {
		messageErr := errLoadErr
		if errors.IsNotFound(err) {
			messageErr = errLoadFound
		}
		return fmt.Errorf(
			messageErr,
			rq.Name,
			rq.Namespace,
			err,
		)
	}
	rq.Quotas = quotas
	rq.IsLoaded = true
	return nil
}

// Resize increment or decrement the Spec.Hard accroding to the increment value.
// Default is 0 and return an error if something is wrong
func (rq *ResourceQuotas) Resize(incrementValue string) error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitResize,
			rq.Name,
			incrementValue,
		),
	)
	if !rq.IsLoaded {
		errLoad := rq.Load()
		if errLoad != nil {
			return errLoad
		}
	}
	incr := sxIncrementor.NewIncrementor(incrementValue)
	newHard := rq.incrementResourceValues(&rq.Quotas.Spec.Hard, incr)
	rq.Quotas.Spec.Hard = *newHard
	return nil
}

// Adjust copy all the Status.Used to the Spec.Hard and
// increment or decrement accroding to the increment value. Default
// is 0 and return an error if something is wrong
func (rq *ResourceQuotas) Adjust(incrementValue string) error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitAdjust,
			rq.Name,
			incrementValue,
		),
	)
	if !rq.IsLoaded {
		errLoad := rq.Load()
		if errLoad != nil {
			return errLoad
		}
	}
	incr := sxIncrementor.NewIncrementor(incrementValue)
	rq.Quotas.Spec.Hard = rq.Quotas.Status.Used.DeepCopy()
	newHard := rq.incrementResourceValues(&rq.Quotas.Spec.Hard, incr)
	rq.Quotas.Spec.Hard = *newHard
	return nil
}

// Merge content from a template into the current resourceQuota
func (rq *ResourceQuotas) MergeTemplate(template sxTpl.Template) error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitMergeTpl,
			template.GetName(),
			rq.Name,
		),
	)
	templateQuota, err := template.GetContentObj()

	if err != nil {
		return fmt.Errorf(errMergeTplFound, err)
	}
	// Get the existing destQuota from the Kubernetes API.
	destQuota, errGet := rq.
		k8sclient.
		Clientset.
		CoreV1().
		ResourceQuotas(rq.Namespace).
		Get(context.TODO(),
			rq.Name,
			metav1.GetOptions{},
		)
	if errGet != nil {
		return fmt.Errorf(
			errMergeTplFoundQ,
			rq.Name,
			rq.Namespace,
			errGet,
		)
	}
	// Merge the ResourceQuotas.
	for k, v := range templateQuota.Spec.Hard {
		if _, found := destQuota.Spec.Hard[k]; !found {
			rq.display.Debug(
				fmt.Sprintf(
					ddMergeTplAddVal,
					k.String(),
					v.String(),
					rq.Name,
				),
			)
			destQuota.Spec.Hard[k] = v
		}
	}
	rq.Quotas = destQuota
	return nil
}

// Resize copy all the Status.Used to the Spec.Hard and
// increment or decrement accroding to the increment value. Default
// is 0 and return an error if something is wrong
func (rq *ResourceQuotas) Apply() error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitApply,
			rq.Name,
			rq.Namespace,
		),
	)
	_, err := rq.
		k8sclient.
		Clientset.
		CoreV1().
		ResourceQuotas(rq.Namespace).
		Update(
			context.TODO(),
			rq.Quotas,
			metav1.UpdateOptions{},
		)
	if err != nil {
		return fmt.Errorf(
			errApplyExec,
			rq.Name,
			rq.Namespace,
			err,
		)
	}
	return nil
}

// Resize copy all the Status.Used to the Spec.Hard and
// increment or decrement accroding to the increment value. Default
// is 0 and return an error if something is wrong
func (rq *ResourceQuotas) Delete() error {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitDelete,
			rq.Name,
			rq.Namespace,
		),
	)
	errDel := rq.
		k8sclient.
		Clientset.
		CoreV1().
		ResourceQuotas(rq.Namespace).
		Delete(
			context.TODO(),
			rq.Name,
			metav1.DeleteOptions{},
		)
	if errDel != nil {
		return fmt.Errorf(
			errDeleteExec,
			rq.Name,
			rq.Namespace,
			errDel,
		)
	}
	return nil
}

// GetYaml return a YAML representation of the Quota
func (rq *ResourceQuotas) GetYaml() string {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitGetYaml,
			rq.Name,
			rq.Namespace,
		),
	)
	yamlData, err := yaml.Marshal(&rq.Quotas)
	if err != nil {
		return ""
	}
	return string(yamlData)
}

// GetYaml return a YAML representation of the Quota
func (rq *ResourceQuotas) Print() {
	rq.display.Debug(
		fmt.Sprintf(
			ddInitPrint,
			rq.Name,
			rq.Namespace,
		),
	)
	fmt.Println(rq.GetYaml())
}

// Get gets the corev1.ResourceQuota from the ResourceQuotas.
func (rq *ResourceQuotas) Get() *corev1.ResourceQuota {
	return rq.Quotas
}

// GetName gets the name from the ResourceQuotas.
func (rq *ResourceQuotas) GetName() string {
	return rq.Name
}

// GetNamespace gets the namespace from the ResourceQuotas.
func (rq *ResourceQuotas) GetNamespace() string {
	return rq.Namespace
}

// GetNamespace gets the namespace from the ResourceQuotas.
func (rq *ResourceQuotas) Debug() *ResourceQuotas {
	rq.display.Debug(ddDebugQtStart)
	rq.display.Debug(fmt.Sprintf(ddDebugQtQuotas, rq.Quotas.GetUID()))
	rq.display.Debug(fmt.Sprintf(ddDebugQtValName, rq.GetName()))
	rq.display.Debug(fmt.Sprintf(ddDebugQtValNamespace, rq.GetNamespace()))
	rq.display.Debug(fmt.Sprintf(ddDebugQtValIsLoaded, rq.IsLoaded))
	for k, v := range rq.Quotas.Spec.Hard {
		rq.display.Debug(fmt.Sprintf(ddDebugQtValQuotaRule, k.String(), v.String()))
	}
	return rq
}

// incrementResourceValues adds the increment to the value of each
// resource in the given list.
func (rq *ResourceQuotas) incrementResourceValues(resources *corev1.ResourceList, increment *sxIncrementor.Incrementor) *corev1.ResourceList {
	rq.display.Debug(ddInitincrementResourceValues)
	newResourceList := corev1.ResourceList.DeepCopy(*resources)
	// increment.Debug()
	// rq.Debug()
	for k, v := range *resources {
		newValue := v.DeepCopy()
		newQuotasRef := NewQuotasDef(k.String(), v.String())
		errorIncr := newQuotasRef.Increment(increment)
		if errorIncr != nil {
			rq.display.Warning(
				fmt.Sprintf(
					warnErrorIncrement,
					k.String(),
					v.String(),
					increment.GetValStringUnit(),
					errorIncr,
				),
			)
			newResourceList[k], _ = resource.ParseQuantity(newValue.String())
		} else {
			newValue = *newQuotasRef.GetValQty()
			switch newQuotasRef.GetValUnit() {
			case "m", "Ki", "Mi", "Gi", "Ti", "Pi":
				newResourceList[k], _ = resource.ParseQuantity(newValue.String() + newQuotasRef.GetValUnit())
			default:
				newResourceList[k], _ = resource.ParseQuantity(newValue.String())
			}
		}
	}
	return &newResourceList
}
