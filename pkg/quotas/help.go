package quotas

import (
	"fmt"

	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
)

// DisplayHelpCmdQuotasCreate display the help message for the
// sxquotas create sub-command
func DisplayHelpCmdQuotasCreate() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the create quotas help message")
	helpMessage := `
create sub-command allow you to create a new resourceQuota
resourceQuota based on the given template. If quotas already exist, 
it will return an error 

Usage: sxquotas create NAME [TEMPLATE] [OPTIONS]...

Arguments:
  NAME       The name of the quotas (mandatory)
  TEMPLATE   The name of the template (optional, use default template if not provided)

Generic options:
  --debug    Activates debug mode for detailed troubleshooting information.
  --help     Displays this help message and exits.

Examples:
  Create the example-quota resourceQuota based on the default template:
  $ sxquotas create example-quota
  Create the ocp-quota resourceQuota based on the openshift template:
  $ sxquotas create ocp-quota openshift

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdQuotasUpdate display the help message for the
// sxquotas update sub-command
func DisplayHelpCmdQuotasUpdate() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the update quotas help message")
	helpMessage := `
  update sub-command allow you to update a new resourceQuota
resourceQuota based on the given template. If quotas already exist, 
it will return an error 

Usage: sxquotas update [NAME] [TEMPLATE] [OPTIONS]...

Arguments:
  NAME                   The name of the resourceQuota to update. (optional, use SXQUOTAS_NAME env var or 'default' if not provided)
  TEMPLATE               The name of the template (optional, use default template if not provided)

Specific options:
  --namespace NAME       Use the NAME namespace. If not provided, use SXQUOTAS_NS env var or current namespace if not provided)
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXQUOTAS_NAME          Define the name of the resourceQuota to use if not set. 
  SXQUOTAS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
Update the example-quota resourceQuota based on the default template:
  $ sxquotas update example-quota
  Update the ocp-quota resourceQuota based on the openshift template:
  $ sxquotas update ocp-quota openshift

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdQuotasCreate display the help message for the
// sxquotas create sub-command
func DisplayHelpCmdQuotasGenerate() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the generate quotas help message")
	helpMessage := `
generate sub-command allow you to generate a new resourceQuota
resourceQuota based on the given template. If template is found, it
will display the resource quota named and without namespace definition.
Ready to be piped into a kubectl apply command.

Usage: sxquotas generate [NAME] [TEMPLATE] [OPTIONS]...

Arguments:
  NAME                   The name of the resourceQuota to generate. (optional, use SXQUOTAS_NAME env var or default if not provided)
  TEMPLATE               The name of the template (optional, use default template if not provided)

Specific options:
  --namespace NAME       Use the NAME namespace. If not provided, use SXQUOTAS_NS env var or current namespace if not provided)

Environment variable:
  SXQUOTAS_NAME          Define the name of the resourceQuota to use if not set. 
  SXQUOTAS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Display the example-quota resourceQuota based on the default template:
  $ sxquotas generate example-quota
  Display the ocp-quota resourceQuota based on the openshift template:
  $ sxquotas generate ocp-quota openshift
  Generate and manualy apply the k8s-quota based on the k8s template:
  $ sxquotas generate k8s-quota k8s | kubectl apply -f -

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdQuotasCreate display the help message for the
// sxquotas create sub-command
func DisplayHelpCmdQuotasApply() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the apply quotas help message")
	helpMessage := `
apply sub-command allow you to apply a template into an existing
resourceQuota. If quotas doesn't exist, it will return an error 

Usage: sxquotas apply [NAME] [TEMPLATE] [OPTIONS]...

Arguments:
  NAME                   The name of the resourceQuota to update. (optional, use SXQUOTAS_NAME env var or default if not provided)
  TEMPLATE               The name of the template (optional, use default template if not provided)

Specific options:
  --namespace NAME       Use the NAME namespace. If not provided, use SXQUOTAS_NS env var or current namespace if not provided)
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXQUOTAS_NAME          Define the name of the resourceQuota to use if not set. 
  SXQUOTAS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Create the example-quota resourceQuota based on the stack-minimal template
  and add network and storage quota to it :
  $ sxquotas create example-quota stack-minimal
  $ // display the content of example-quota
  $ kubectl describe resourcequota example-quota
  $ // add the storage content to it
  $ sxquotas apply example-quota stack-storage
  $ // display the content of example-quota
  $ kubectl describe resourcequota example-quota
  $ // add the network content to it
  $ sxquotas apply example-quota stack-network
  $ // display the content of example-quota
  $ kubectl describe resourcequota example-quota

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdQuotasResize display the help message for the
// sxquotas resize sub-command
func DisplayHelpCmdQuotasResize() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the resize quotas help message")
	helpMessage := `
resize sub-command allow you to resize an existing resourceQuota
resourceQuota based on the given template

Usage: sxquotas resize INCREMENT [NAME] [OPTIONS]...

Arguments:
  INCREMENT              The value of the increment (optional)
                         it could be a positive or negative integer or percentage
                         if not provided, resize will increment by 1
  NAME                   The name of the resourceQuota to update. (optional, use SXQUOTAS_NAME env var or default if not provided)

Specific options:
  --namespace NAME       Use the NAME namespace. If not provided, use SXQUOTAS_NS env var or current namespace if not provided)
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXQUOTAS_NAME          Define the name of the resourceQuota to use if not set. 
  SXQUOTAS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Resize the example-quota resourceQuota of 20% :
  $ sxquotas resize example-quota 20%
  Resize the example-quota resourceQuota of 3 :
  $ sxquotas resize ocp-quota 3

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdQuotasAdjust display the help message for the
// sxquotas adjust sub-command
func DisplayHelpCmdQuotasAdjust() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the adjust quotas help message")
	helpMessage := `
adjust sub-command allow you to adjust an existing resourceQuota
resourceQuota based on the status.used informations

Usage: sxquotas adjust INCREMENT [NAME] [OPTIONS]...

Arguments:
  INCREMENT              The value of the increment (optional)
                         it could be a positive or negative integer or percentage
                         if not provided, use 0 and adjusting will be done based only 
                         on the status.used informations of the resourceQuota
  NAME                   The name of the resourceQuota to update. (optional, use SXQUOTAS_NAME env var or default if not provided)

Specific options:
  --namespace NAME       Use the NAME namespace. If not provided, use SXQUOTAS_NS env var or current namespace if not provided)
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXQUOTAS_NAME          Define the name of the resourceQuota to use if not set. 
  SXQUOTAS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Adjust the example-quota resourceQuota of 20% :
  $ sxquotas adjust example-quota 20%
  Adjust the example-quota resourceQuota of 3 :
  $ sxquotas adjust ocp-quota 3

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdExport display the help message for the
// sxquotas export sub-command
func DisplayHelpCmdExport() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the export quotas help message")
	helpMessage := `
export sub-command allow you to export all resourceQuota present in
the current namespace into a csv file. 

Usage: sxquotas export [NS_PATTERN] [OPTIONS]...

Arguments:
  NS_PATTERN             A regex to used for multiple namespace selection. (optional, use SXQUOTAS_NS env var or current namespace if not provided)

Specific options:
  --no-header            Return no header (default return headers)
  --sep SEPARATOR        Use this separator instead of the default ';'
  --output FILE          Output content into the given file instead of stdout
  --format FORMAT        Define the output format. could be 'detail', 'namespace' or 'resource' (default is detail)
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXQUOTAS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Export quotas from the current namespace :
  $ sxquotas export
  Export all quotas defined in namespaces starting by 'myproject-' :
  $ sxquotas export myproject-*
  Export all quotas with separator '-' into /tmp/sxquotas.csv :
  $ sxquotas export '*' --sep - --output /tmp/sxquotas.csv

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdDescribe display the help message for the
// sxquotas describe sub-command
func DisplayHelpCmdDescribe() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the describe quotas help message")
	helpMessage := `
describe sub-command allow you to display all resourceQuota present in
the current namespace into a terminal based command. 

Usage: sxquotas describe [NS_PATERN] [OPTIONS]...

Arguments:
  NS_PATTERN             A regex to used for multiple namespace selection. (optional, use SXQUOTAS_NS env var or current namespace if not provided)

Specific options:
  --format FORMAT        Define the output format. could be 'detail', 'namespace' or 'resource' (default is detail)
  --kubeconfig FILEPATH  Use the file located at FILEPATH as the kubeconfig to use. If not provided, use KUBECONFIG env var or user home .kube/config if not provided)

Environment variable:
  SXQUOTAS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Examples:
  Describe quotas from the current namespace :
  $ sxquotas describe
  Describe all quotas defined in namespaces starting by 'myproject-' :
  $ sxquotas describe myproject-*
  Describe all quotas by namespace :
  $ sxquotas describe '*' --format namespace

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}

// DisplayHelpCmdDescribe display the help message for the
// sxlimits describe sub-command
func DisplayHelpCmd() {
	display := sxUtils.NewCmdDisplay(GroupName)
	display.Debug("Display the sxlimits help message")
	helpMessage := `
sxquotas allow you to interact and observe resourcequota into your
running kubernetes cluster. 

Usage: sxquotas SUBCOMMAND [OPTIONS]...

Sub-commands:
- templates              Get the list of available templates
- template               Get information about a template
- generate               Generate a new resourcequota based on a template
- create                 Create a new resourcequota based on the given template
- apply                  Apply a template with the given resourcequota
- update                 Merge a template with the given resourcequota
- resize                 Resize resourcequota definitions
- adjust                 Adjust resourcequota according to running workload
- describe               Display the resourcequotas
- export                 Export resourcequota into csv content
- version                Get the version of this package

Environment variable:
  SXQUOTAS_NS            Define the namespace to use instead of the current namespace.

Generic options:
  --debug                Activates debug mode for detailed troubleshooting information.
  --help                 Displays this help message and exits.

Note: The '--debug' and '--help' options are applicable to all commands for enhanced functionality or information.
`
	fmt.Println(helpMessage)
}
