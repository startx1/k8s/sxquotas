/*
Copyright 2021 Startx, member of LaHSC

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package quotas

import (
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"

	sxIncrementor "gitlab.com/startx1/k8s/go-libs/pkg/incrementor"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	"k8s.io/apimachinery/pkg/api/resource"
)

const (
	// GroupNameDef is the group name use in this package
	GroupNameDef                 = "quotas.def"
	ddQdfInitQdfNewQuotasDef     = "Init QuotasDef object"
	ddQdfInitQdfnewQuotasDef     = "Init a new QuotasDef object"
	ddQdfInitQdfLoadFromString   = "Load quotasDef %s : %s"
	ddQdfIncrementStart          = "Start incrementing %s by %s"
	ddDebugQdfStart              = "DEBUG the quotasDef"
	ddDebugQdfKey                = "DEBUG -          key : %s"
	ddDebugQdfValString          = "DEBUG -   val string : %s"
	ddDebugQdfValInt             = "DEBUG -      val int : %d"
	ddDebugQdfValQty             = "DEBUG - val quantity : %s"
	ddDebugQdfValType            = "DEBUG -         type : %s"
	ddDebugQdfValUnit            = "DEBUG -         unit : %s"
	ddQdfIncrementRV             = "%s : increment : %s => %s [%s]"
	ddQdfIncrementDetectMem      = "%s : Detected a mem/sto quota (%s)"
	ddQdfIncrementDetectCpu      = "%s : Detected a cpu quota (%s)"
	ddQdfIncrementDetectDef      = "%s : Detected a default quota (%s)"
	ddQdfIncrementDetectNegative = "%s : move from %d to %d (not negative)"
	ddQdfIncrementDoMem          = "%s : MEM move from %s to %s [%s]"
	ddQdfIncrementDoCpu          = "%s : CPU move from %s to %s [%s]"
	ddQdfIncrementDoDef          = "%s : NUM move from %s to %s [%s]"
	errQdfLoadParse              = "error parsing %s value : %v"
)

//
// Define object variables
//

// K8sClient represents a wrapper around the Kubernetes clientset.
type QuotasDef struct {
	key       string
	valString string
	valInt    int
	valQty    resource.Quantity
	valType   string
	valUnit   string
	display   *sxUtils.CmdDisplay
}

// NewQuotasDef create a new QuotasDef ready to use the clientset
// to interact with the kubernetes cluster
func NewQuotasDef(key string, val string) *QuotasDef {
	// sxUtils.DisplayDebug(GroupNameDef,ddQdfInitQdfNewQuotasDef)
	qdf := newQuotasDef()
	qdf.LoadFromString(key, val)
	return qdf
}

// newQuotasDef create a new QuotasDef with default (0) value set
func newQuotasDef() *QuotasDef {
	// sxUtils.DisplayDebug(GroupNameDef,ddQdfInitQdfnewQuotasDef)
	qty, _ := resource.ParseQuantity("0")
	qdf := &QuotasDef{
		key:       "pod",
		valString: "0",
		valInt:    0,
		valQty:    qty,
		valType:   "count",
		valUnit:   "int",
		display:   sxUtils.NewCmdDisplay(GroupNameDef),
	}
	return qdf
}

// DeepCopy creates a deep copy of an QuotasDef
func (qdf *QuotasDef) DeepCopy() *QuotasDef {
	copiedQty := qdf.valQty.DeepCopy()
	return &QuotasDef{
		key:       qdf.key,
		valInt:    qdf.valInt,
		valType:   qdf.valType,
		valUnit:   qdf.valUnit,
		valString: qdf.valString,
		valQty:    copiedQty,
	}
}

// NewQuotasDef create a new QuotasDef ready to use the clientset
// to interact with the kubernetes cluster
func (qdf *QuotasDef) LoadFromString(key string, val string) *QuotasDef {
	qdf.display.Debug(fmt.Sprintf(ddQdfInitQdfLoadFromString, key, val))
	qdf.key = key
	qdf.valString = val
	qdf.valType = "count"
	qdf.valUnit = ""
	factor := 1
	re := regexp.MustCompile("(-?[0-9]+)([a-zA-Z]*)$")
	matches := re.FindStringSubmatch(qdf.GetValString())
	// Check if the string contains ".cpu"
	if strings.Contains(qdf.key, "/gpu") ||
		strings.Contains(qdf.key, "/cpu") ||
		strings.Contains(qdf.key, ".cpu") {
		qdf.SetValType("cpu")
		if len(matches) > 2 && matches[2] == "m" {
			qdf.valUnit = matches[2]
			qdf.valString = matches[1]
		} else {
			qdf.valUnit = "m"
			qdf.valString = matches[1]
			iint, errParsei := strconv.Atoi(qdf.valString)
			if errParsei != nil {
				qdf.display.Error(
					fmt.Sprintf(
						errQdfLoadParse,
						qdf.GetValString(),
						errParsei),
				)
			}
			qdf.valInt = (iint * 1000)
			qdf.valString = fmt.Sprint(qdf.valInt)
		}
	} else {
		if len(matches) > 2 {
			qdf.valUnit = matches[2]
			qdf.valString = matches[1]
			switch qdf.GetValUnit() {
			case "m":
				qdf.SetValType("cpu")
			case "Pi":
				qdf.SetValType("mem")
				factor = 1024 * 1024 * 1024 * 1024 * 1024
			case "Ti":
				qdf.SetValType("mem")
				factor = 1024 * 1024 * 1024 * 1024
			case "Gi":
				qdf.SetValType("mem")
				factor = 1024 * 1024 * 1024
			case "Mi":
				qdf.SetValType("mem")
				factor = 1024 * 1024
			case "Ki":
				qdf.SetValType("mem")
				factor = 1024
			default:
				qdf.SetValType("count")
			}
		}
	}

	iint, errParsei := strconv.Atoi(qdf.GetValString())
	if errParsei != nil {
		qdf.display.ExitError(
			fmt.Sprintf(
				errQdfLoadParse,
				qdf.GetValString(),
				errParsei),
			30)
		return nil
	}
	qdf.valInt = (iint * factor)
	qty, errParse := resource.ParseQuantity(qdf.GetValString())
	if errParse != nil {
		qdf.display.ExitError(
			fmt.Sprintf(
				errQdfLoadParse,
				qdf.GetValString(),
				errParse),
			30)
		return nil
	}
	qdf.valQty = qty
	return qdf
}

// Display the content of the QuotasDef
func (qdf *QuotasDef) Debug() *QuotasDef {
	qdf.display.Debug(ddDebugQdfStart)
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfKey, qdf.GetKey()))
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfValString, qdf.valString))
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfValInt, qdf.valInt))
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfValQty, qdf.valQty.String()))
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfValType, qdf.valType))
	qdf.display.Debug(fmt.Sprintf(ddDebugQdfValUnit, qdf.valUnit))
	return qdf
}

// Display the content of the QuotasDef
func (qdf *QuotasDef) Increment(increment *sxIncrementor.Incrementor) error {
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfIncrementStart,
			qdf.GetKey(),
			increment.GetValStringUnit(),
		),
	)
	oldValue := qdf.GetValString()
	switch qdf.GetKey() {
	case
		"requests.memory",
		"limits.memory",
		"requests.storage",
		"ephemeral-storage":
		qdf.display.Debug(
			fmt.Sprintf(
				ddQdfIncrementDetectMem,
				qdf.GetKey(),
				increment.GetValStringUnit(),
			),
		)
		qdf.incrementMemory(increment)
	case
		"requests.cpu",
		"limits.cpu":
		qdf.display.Debug(
			fmt.Sprintf(
				ddQdfIncrementDetectCpu,
				qdf.GetKey(),
				increment.GetValStringUnit(),
			),
		)
		qdf.incrementCPU(increment)
	default:
		qdf.display.Debug(
			fmt.Sprintf(
				ddQdfIncrementDetectDef,
				qdf.GetKey(),
				increment.GetValStringUnit(),
			),
		)
		qdf.incrementDefault(increment)
	}
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfIncrementRV,
			qdf.GetKey(),
			oldValue,
			qdf.GetValString(),
			increment.GetValStringUnit(),
		),
	)
	return nil
}

// increment the CPU using sxIncrementor.
func (qdf *QuotasDef) incrementCPU(increment *sxIncrementor.Incrementor) *QuotasDef {
	oldValue := qdf.GetValString()
	iint, errParsei := strconv.Atoi(qdf.valString)
	if errParsei != nil {
		qdf.display.ExitError(
			fmt.Sprintf(errQdfLoadParse, qdf.valString, errParsei),
			30,
		)
		return nil
	}
	newValueCalc := (iint + increment.GetValInt())
	if increment.IsPercentage() {
		result := float64(iint) * (float64(increment.GetValInt()) / 100)
		newValueCalc = int(math.Ceil(result))
	} else {
		if qdf.GetValUnit() == "m" {
			newValueCalc = (iint + (increment.GetValInt() * 1000))
		}
	}
	newValueString := fmt.Sprintf("%d%s", newValueCalc, qdf.GetValUnit())
	qdf.LoadFromString(qdf.key, newValueString)
	newValueQty := qdf.GetValQty()
	if newValueQty.Sign() < 0 {
		newValueQty.Set(0)
		qdf.display.Debug(
			fmt.Sprintf(
				ddQdfIncrementDetectNegative,
				qdf.GetKey(),
				newValueQty.Value(),
				0,
			),
		)
		qdf.SetValQty(newValueQty)
	}
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfIncrementDoCpu,
			qdf.GetKey(),
			oldValue,
			qdf.GetValString(),
			increment.GetValStringUnit(),
		),
	)
	return qdf
}

// increment the Memory/storage using sxIncrementor.
func (qdf *QuotasDef) incrementMemory(increment *sxIncrementor.Incrementor) *QuotasDef {
	oldValue := qdf.GetValString()
	iint, errParsei := strconv.Atoi(qdf.valString)
	if errParsei != nil {
		qdf.display.ExitError(
			fmt.Sprintf(errQdfLoadParse, qdf.valString, errParsei),
			30,
		)
		return nil
	}
	newValueCalc := (iint + increment.GetValInt())
	if increment.IsPercentage() {
		result := float64(iint) * (float64(increment.GetValInt()) / 100)
		newValueCalc = int(math.Ceil(result))
	}
	newValueString := fmt.Sprintf("%d%s", newValueCalc, qdf.GetValUnit())
	qdf.LoadFromString(qdf.key, newValueString)
	newValueQty := qdf.GetValQty()
	if newValueQty.Sign() < 0 {
		newValueQty.Set(0)
		qdf.display.Debug(
			fmt.Sprintf(
				ddQdfIncrementDetectNegative,
				qdf.GetKey(),
				newValueQty.Value(),
				0,
			),
		)
		qdf.SetValQty(newValueQty)
	}
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfIncrementDoMem,
			qdf.GetKey(),
			oldValue,
			qdf.GetValString(),
			increment.GetValStringUnit(),
		),
	)
	return qdf
}

// increment the Memory/storage using sxIncrementor.
func (qdf *QuotasDef) incrementDefault(increment *sxIncrementor.Incrementor) *QuotasDef {
	oldValue := qdf.GetValString()
	iint, errParsei := strconv.Atoi(qdf.valString)
	if errParsei != nil {
		qdf.display.ExitError(fmt.Sprintf(errQdfLoadParse, qdf.valString, errParsei), 30)
		return nil
	}
	newValueCalc := (iint + increment.GetValInt())
	if increment.IsPercentage() {
		result := float64(iint) * (float64(increment.GetValInt()) / 100)
		newValueCalc = int(math.Ceil(result))
	}
	newValueString := fmt.Sprintf("%d%s", newValueCalc, qdf.GetValUnit())
	qdf.LoadFromString(qdf.key, newValueString)
	newValueQty := qdf.GetValQty()
	if newValueQty.Sign() < 0 {
		newValueQty.Set(0)
		qdf.display.Debug(
			fmt.Sprintf(
				ddQdfIncrementDetectNegative,
				qdf.GetKey(),
				newValueQty.Value(),
				0,
			),
		)
		qdf.SetValQty(newValueQty)
	}
	qdf.display.Debug(
		fmt.Sprintf(
			ddQdfIncrementDoDef,
			qdf.GetKey(),
			oldValue,
			qdf.GetValString(),
			increment.GetValStringUnit(),
		),
	)
	return qdf
}

// SetKey sets the key valueQty.
func (qdf *QuotasDef) SetKey(key string) *QuotasDef {
	qdf.key = key
	return qdf
}

// GetKey returns the GetKey valueQty.
func (qdf *QuotasDef) GetKey() string {
	return qdf.key
}

// SetValString sets the valString field.
func (qdf *QuotasDef) SetValString(val string) *QuotasDef {
	qdf.LoadFromString(qdf.GetKey(), val)
	return qdf
}

// GetValString returns the valString field.
func (qdf *QuotasDef) GetValString() string {
	return qdf.valString
}

// SetValInt sets the valInt field.
func (qdf *QuotasDef) SetValInt(val int) *QuotasDef {
	valString := strconv.Itoa(val)
	qdf.LoadFromString(qdf.GetKey(), valString)
	return qdf
}

// GetValInt returns the valInt field.
func (qdf *QuotasDef) GetValInt() int {
	return qdf.valInt
}

// SetValQty sets the valQty field.
func (qdf *QuotasDef) SetValQty(val *resource.Quantity) *QuotasDef {
	qdf.LoadFromString(qdf.GetKey(), val.String())
	return qdf
}

// GetValQty returns the valQty field.
func (qdf *QuotasDef) GetValQty() *resource.Quantity {
	return &qdf.valQty
}

// SetValType sets the valType field.
func (qdf *QuotasDef) SetValType(valType string) *QuotasDef {
	qdf.valType = valType
	return qdf
}

// GetValType returns the valType field.
func (qdf *QuotasDef) GetValType() string {
	return qdf.valType
}

// SetValUnit sets the valUnit field.
func (qdf *QuotasDef) SetValUnit(valUnit string) *QuotasDef {
	qdf.valUnit = valUnit
	return qdf
}

// GetValUnit returns the valUnit field.
func (qdf *QuotasDef) GetValUnit() string {
	return qdf.valUnit
}
