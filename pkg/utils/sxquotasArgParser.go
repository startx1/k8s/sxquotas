package utils

import (
	"context"
	"errors"
	"flag"
	"fmt"

	sxKCli "gitlab.com/startx1/k8s/go-libs/pkg/k8sclient"
	sxKCfg "gitlab.com/startx1/k8s/go-libs/pkg/kubeconfig"
	sxUtils "gitlab.com/startx1/k8s/go-libs/pkg/utils"
	sxQuotas "gitlab.com/startx1/k8s/sxquotas/pkg/quotas"
	sxTpl "gitlab.com/startx1/k8s/sxquotas/pkg/templates"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	// GroupNameSXQuotas is the group name use in this package
	GroupNameSXQuotas                = "utils.sxquotas"
	ddDebugSXQAPPrepare              = "Prepare the sxQuotasArgParser"
	ddDebugSXQAPPrepareAction        = "Found the action %s"
	ddDebugSXQAPPrepareSubAction     = "Found the sub-action %s"
	ddDebugSXQAPPrepareNeedTemplates = "action %s need templates : %t"
	ddDebugSXQAPPrepareNeedK8sClient = "action %s need k8sclient : %t"
	ddDebugSXQAPPrepareHasNS         = "sxQuotasArgParser has NS  : %t (%s)"
	ddDebugSXQAPPrepareHasKConfig    = "sxQuotasArgParser has Kubeconfig  : %t (%s)"
	ddDebugSXQAPExec                 = "Exec the sxQuotasArgParser"
	ddDebugSXQAPStart                = "Start display the sxQuotasArgParser"
	errSXQAPTemplateReq              = "template name is required for 'template' command"
	errSXQAPPrepare                  = "error executing the SXQuotas cli parser : %v"
	msgExecExportRecordFile          = "Recorded all resourceQuotas found in %s namespaces into %s"
	msgExecDescribeOutput            = "Describe all resourceQuotas"
	// imported from main
	ddSeqInit            = "Start the main function"
	ddSeqStart           = "Executing %s subcommand"
	ddCreateStart        = "Start creating the %s quota (based on %s template)"
	ddUpdateStart        = "Start updating the %s quota (based on %s template)"
	ddAdjustStart        = "Start adjusting the %s quota with %s"
	ddResizeStart        = "Start resizing the %s quota with %s"
	ddOkCreate           = "Quotas %s is added into namespace %s"
	ddOkApply            = "Quotas %s is updated into namespace %s"
	ddOkGenerate         = "Quotas %s is generated"
	ddOkResize           = "Quotas %s is resized into namespace %s"
	ddOkAdjust           = "Quotas %s is adjusted into namespace %s"
	errNoSubcmd          = "Expected 'templates', 'template', 'create', 'adjust', 'resize' or 'export' subcommands. See : sxquotas help."
	errTemplateReq       = "Template name is required for 'template' command"
	errQuotasNameReq     = "Quota name is required for '%s' command"
	errCreateGetTpl      = "Error accessing the template : %v"
	errUpdateGetTpl      = "Error accessing the template : %v"
	errResizeReq         = "resourceQuota name is required for '%s' command"
	errResizeLoad        = "Error loading resourceQuota %s in namespace %s : %v"
	errResizeResize      = "Error resizing resourceQuota %s in namespace %s : %v"
	errResizeApply       = "Error applying resourceQuota %s in namespace %s : %v"
	errAdjustReq         = "Quota name is required for 'adjust' command"
	errAdjustLoad        = "Error loading resourceQuota %s in namespace %s : %v"
	errAdjustAdjust      = "Error adjusting resourceQuota %s in namespace %s : %v"
	errAdjustApply       = "Error applying resourceQuota %s in namespace %s : %v"
	errApplyLoad         = "Error loading resourceQuota %s in namespace %s : %v"
	errApplyMerge        = "Error resizing resourceQuota %s in namespace %s : %v"
	errExportDo          = "Error exporting resourceQuota %s in namespace %s : %v"
	errApplyApply        = "Error applying resourceQuota %s in namespace %s : %v"
	errCreateGenerate    = "Error generating resourceQuota %s : %v"
	errApply             = "Error applying resourceQuota %s into the namespace %s : %v"
	errCreate            = "Error creating resourceQuota %s into the namespace %s : %v"
	errUpdateTemplateReq = "Template name is required for 'update' command"
)

// ArgParser is a definition for displaying message for a command line
type sxQuotasArgParser struct {
	Args          *sxUtils.ArgParser
	Templates     interface{}
	K8sclient     interface{}
	HasNamespace  bool
	Namespace     string
	HasKubeconfig bool
	Kubeconfig    string
}

// Initialize a ArgParser object
// ex:
//
//	display := NewArgParser("main",true)
func NewSXQuotasArgParser(sourceArgs []string) *sxQuotasArgParser {
	argParser := &sxQuotasArgParser{
		Args:         sxUtils.NewArgParser(sourceArgs),
		Templates:    nil,
		K8sclient:    nil,
		HasNamespace: false,
		Namespace:    "",
	}
	return argParser
}

// Display the content of the ArgParser
func (argParser *sxQuotasArgParser) Debug() *sxQuotasArgParser {
	display := sxUtils.NewCmdDisplay(GroupNameSXQuotas)
	display.Debug(ddDebugSXQAPStart)
	argParser.Args.Debug()
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareHasNS, argParser.HasNamespace, argParser.Namespace))
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareHasKConfig, argParser.HasKubeconfig, argParser.Kubeconfig))
	return argParser
}

// used to scan arguments remove the globals one and return a new arguments
// list without them
func (argParser *sxQuotasArgParser) Check() error {
	args := argParser.Args
	if len(args.ParsedArgs) <= 1 {
		return nil
	} else {
		if args.IsHelp {
			return nil
		}
		switch args.TopAction {
		case "template":
			if len(args.ParsedArgs) < 3 {
				return errors.New(errSXQAPTemplateReq)
			}
		default:
			return nil
		}
	}
	return nil
}

// used to scan arguments remove the globals one and return a new arguments
// list without them
func (argParser *sxQuotasArgParser) Prepare() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXQuotas)
	needTemplates := false
	needK8sClient := false
	display.Debug(ddDebugSXQAPPrepare)
	args := argParser.Args
	errCheck := argParser.Check()
	if errCheck != nil {
		display.ExitError(
			fmt.Sprintf(
				errSXQAPPrepare,
				errCheck,
			),
			30,
		)
	}
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareAction, args.TopAction))
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareSubAction, args.SubAction))
	switch args.TopAction {
	case "templates", "template", "generate":
		needTemplates = true
	case "update", "create", "apply":
		needTemplates = true
		needK8sClient = true
	case "resize", "adjust", "export", "describe":
		needK8sClient = true
	}
	// check global flags namespace and kubeconfig
	if argParser.Args.HasFlag("--namespace") {
		argParser.HasNamespace = true
		argParser.Namespace = argParser.Args.GetFlagNextVal("--namespace")
		nsPos := argParser.Args.GetFlagPos("--namespace")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
	}
	if argParser.Args.HasFlag("--kubeconfig") {
		argParser.HasKubeconfig = true
		argParser.Kubeconfig = argParser.Args.GetFlagNextVal("--kubeconfig")
		nsPos := argParser.Args.GetFlagPos("--kubeconfig")
		argParser.Args.RemoveFlagFromPos(nsPos)
		argParser.Args.RemoveFlagFromPos(nsPos)
	}
	// load templates if needed
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareNeedTemplates, args.TopAction, needTemplates))
	if needTemplates {
		argParser.Templates = sxTpl.NewTemplateStack()
	}
	// load k8sclient if needed
	display.Debug(fmt.Sprintf(ddDebugSXQAPPrepareNeedK8sClient, args.TopAction, needK8sClient))
	forceInCluster := true
	if needK8sClient {
		kubeconfig := sxKCfg.NewKubeConfig()
		if argParser.HasKubeconfig {
			kubeconfig.LoadFromFile(argParser.Kubeconfig)
			forceInCluster = false
		} else {
			err := kubeconfig.Load(true)
			if err == nil {
				forceInCluster = false
			}
		}
		argParser.K8sclient = sxKCli.NewK8sClient(kubeconfig.GetPath(), false, forceInCluster)
		if !argParser.HasNamespace {
			argParser.Namespace = argParser.K8sclient.(*sxKCli.K8sClient).GetCurrentNamespace()
		}
	}
	return nil
}

// used to scan arguments remove the globals one and return a new arguments
// list without them
func (argParser *sxQuotasArgParser) Exec() error {
	display := sxUtils.NewCmdDisplay(GroupNameSXQuotas)
	display.Debug(ddDebugSXQAPExec)
	args := argParser.Args

	createCmd := flag.NewFlagSet("create", flag.ExitOnError)
	// Check for main subcommands
	if len(args.ParsedArgs) <= 1 {
		sxQuotas.DisplayHelpCmd()
	} else {
		switch args.TopAction {
		case "templates":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			argParser.Templates.(*sxTpl.TemplateStack).DisplayTemplates()
		case "template":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			if args.IsHelp {
				sxTpl.DisplayHelpCmdTemplate()
			} else {
				if len(args.ParsedArgs) < 3 {
					display.ExitError(errTemplateReq, 20)
				}
				argParser.Templates.(*sxTpl.TemplateStack).DisplayTemplateInfo(args.SubAction)
			}
		case "update":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			quotaName := "myquotas"
			templateName := "default"
			if args.IsHelp {
				sxQuotas.DisplayHelpCmdQuotasUpdate()
			} else {
				if len(args.ParsedArgs) < 3 {
					display.ExitError(fmt.Sprintf(errQuotasNameReq, args.TopAction), 20)
				}
				quotaName = args.SubAction
				createCmd.StringVar(&templateName, "template", "default", "Template name (optional)")
				createCmd.Parse(args.ParsedArgs[3:])
				if len(args.ParsedArgs) > 3 {
					templateName = args.ParsedArgs[3]
				}
				display.Debug(fmt.Sprintf(ddUpdateStart, quotaName, templateName))
				template, err := argParser.Templates.(*sxTpl.TemplateStack).GetTemplate(templateName)
				if err != nil {
					display.ExitError(fmt.Sprintf(errUpdateGetTpl, err.Error()), 30)
				}
				// get current namespace
				k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
				if !argParser.HasNamespace {
					argParser.Namespace = argParser.K8sclient.(*sxKCli.K8sClient).GetCurrentNamespace()
				}
				rq := sxQuotas.NewRQ(quotaName, argParser.Namespace, k8sClient)
				errLoad := rq.Load()
				if errLoad != nil {
					fmt.Printf(errApplyLoad, quotaName, argParser.Namespace, errLoad)
					return nil
				}
				errMerge := rq.MergeTemplate(*template)
				if errMerge != nil {
					fmt.Printf(errApplyMerge, quotaName, argParser.Namespace, errMerge)
					return nil
				}
				errApply := rq.Apply()
				if errApply != nil {
					fmt.Printf(errApplyApply, quotaName, argParser.Namespace, errApply)
					return nil
				}
				display.Info(fmt.Sprintf(ddOkApply, quotaName, argParser.Namespace))
			}
		case "create", "apply", "generate":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			quotaName := "myquotas"
			templateName := "default"
			if args.IsHelp {
				switch args.TopAction {
				case "apply":
					sxQuotas.DisplayHelpCmdQuotasApply()
				case "generate":
					sxQuotas.DisplayHelpCmdQuotasGenerate()
				default:
					sxQuotas.DisplayHelpCmdQuotasCreate()
				}
				return nil
			} else {
				if len(args.ParsedArgs) < 3 {
					display.ExitError(fmt.Sprintf(errQuotasNameReq, args.TopAction), 20)
				}
				quotaName = args.SubAction
				createCmd.StringVar(&templateName, "template", "default", "Template name (optional)")
				createCmd.Parse(args.ParsedArgs[3:])
				if len(args.ParsedArgs) > 3 {
					templateName = args.ParsedArgs[3]
				}
				display.Debug(fmt.Sprintf(ddCreateStart, quotaName, templateName))
				template, err := argParser.Templates.(*sxTpl.TemplateStack).GetTemplate(templateName)
				if err != nil {
					display.ExitError(fmt.Sprintf(errCreateGetTpl, err.Error()), 30)
				}
				if args.TopAction == "generate" {
					// generate the new quotas from the template
					newQuotas, err := template.GenerateQuotasYaml(quotaName)
					if err != nil {
						display.ExitError(fmt.Sprintf(errCreateGenerate, quotaName, err.Error()), 30)
					}
					// report it's done for debug
					display.Debug(fmt.Sprintf(ddOkGenerate, quotaName))
					// Display yaml result and terminate
					fmt.Println(newQuotas)
					return nil
				} else if args.TopAction == "apply" {
					// get current namespace
					k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
					if !argParser.HasNamespace {
						argParser.Namespace = k8sClient.GetCurrentNamespace()
					}
					rq := sxQuotas.NewRQ(quotaName, argParser.Namespace, k8sClient)
					errLoad := rq.Load()
					if errLoad != nil {
						fmt.Printf(errApplyLoad, quotaName, argParser.Namespace, errLoad)
						return nil
					}
					errMerge := rq.MergeTemplate(*template)
					if errMerge != nil {
						fmt.Printf(errApplyMerge, quotaName, argParser.Namespace, errMerge)
						return nil
					}
					errApply := rq.Apply()
					if errApply != nil {
						fmt.Printf(errApplyApply, quotaName, argParser.Namespace, errApply)
						return nil
					}
					// Inform end user it's done
					display.Info(fmt.Sprintf(ddOkApply, quotaName, argParser.Namespace))
				} else {
					// get current namespace
					k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
					if !argParser.HasNamespace {
						argParser.Namespace = k8sClient.GetCurrentNamespace()
					}
					// generate the new quotas
					newQuotas, errCrt := template.GenerateQuotas(quotaName)
					if errCrt != nil {
						display.ExitError(fmt.Sprintf(errCreate, quotaName, argParser.Namespace, errCrt), 30)
					}
					// create the new quotas into the kubernetes cluster
					_, errK8s := k8sClient.Clientset.CoreV1().ResourceQuotas(argParser.Namespace).
						Create(
							context.Background(),
							&newQuotas,
							v1.CreateOptions{},
						)
					if errK8s != nil {
						display.ExitError(fmt.Sprintf(errCreate, quotaName, argParser.Namespace, errK8s), 30)
					}
					// Inform end user it's done
					display.Info(fmt.Sprintf(ddOkCreate, quotaName, argParser.Namespace))
				}
			}
		case "resize", "adjust":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			msgErrReq := fmt.Sprintf(errResizeReq, args.TopAction)
			msgErrLoad := errResizeLoad
			msgErrAction := errResizeResize
			msgErrApply := errResizeApply
			msgDisplayAction := ddResizeStart
			msgDDOk := ddOkResize
			if args.IsHelp {
				switch args.TopAction {
				case "apply":
					sxQuotas.DisplayHelpCmdQuotasAdjust()
				default:
					sxQuotas.DisplayHelpCmdQuotasResize()
				}
				return nil
			} else {
				if args.TopAction == "adjust" {
					msgErrReq = errAdjustReq
					msgErrLoad = errAdjustLoad
					msgErrAction = errAdjustAdjust
					msgErrApply = errAdjustApply
					msgDisplayAction = ddAdjustStart
					msgDDOk = ddOkAdjust
				}

				if len(args.ParsedArgs) < 3 {
					display.ExitError(msgErrReq, 20)
				}
				quotaName := args.SubAction
				increment := "0"
				if len(args.ParsedArgs) > 3 {
					increment = args.ParsedArgs[3]
				}
				display.Debug(fmt.Sprintf(msgDisplayAction, quotaName, increment))
				k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
				if !argParser.HasNamespace {
					argParser.Namespace = k8sClient.GetCurrentNamespace()
				}
				rq := sxQuotas.NewRQ(quotaName, argParser.Namespace, k8sClient)
				errLoad := rq.Load()
				if errLoad != nil {
					display.ExitError(fmt.Sprintf(msgErrLoad, quotaName, argParser.Namespace, errLoad), 30)
					return nil
				}
				if args.TopAction == "adjust" {
					errAction := rq.Adjust(increment)
					if errAction != nil {
						display.ExitError(fmt.Sprintf(msgErrAction, quotaName, argParser.Namespace, errAction), 30)
						return nil
					}
				} else {
					errAction := rq.Resize(increment)
					if errAction != nil {
						display.ExitError(fmt.Sprintf(msgErrAction, quotaName, argParser.Namespace, errAction), 30)
						return nil
					}
				}
				errApply := rq.Apply()
				if errApply != nil {
					fmt.Println(msgErrApply, quotaName, argParser.Namespace, errApply)
					return nil
				}
				// Inform end user it's done
				display.Info(fmt.Sprintf(msgDDOk, rq.Name, rq.Namespace))
			}

		case "export":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			if args.IsHelp {
				sxQuotas.DisplayHelpCmdExport()
			} else {
				k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
				hasHeader := true
				separator := ";"
				outputFile := "-"
				outputFormat := "detail"
				if argParser.Args.HasFlag("--no-header") {
					hasHeader = false
					argParser.Args.RemoveFlagFromPos(argParser.Args.GetFlagPos("--no-header"))
				}
				if argParser.Args.HasFlag("--sep") {
					separator = argParser.Args.GetFlagNextVal("--sep")
					nsPos := argParser.Args.GetFlagPos("--sep")
					argParser.Args.RemoveFlagFromPos(nsPos)
					argParser.Args.RemoveFlagFromPos(nsPos)
				}
				if argParser.Args.HasFlag("--output") {
					outputFile = argParser.Args.GetFlagNextVal("--output")
					nsPos := argParser.Args.GetFlagPos("--output")
					argParser.Args.RemoveFlagFromPos(nsPos)
					argParser.Args.RemoveFlagFromPos(nsPos)
				}
				if argParser.Args.HasFlag("--format") {
					outputFormat = argParser.Args.GetFlagNextVal("--format")
					nsPos := argParser.Args.GetFlagPos("--format")
					argParser.Args.RemoveFlagFromPos(nsPos)
					argParser.Args.RemoveFlagFromPos(nsPos)
				}

				export, errExport := sxQuotas.RQExporter(args.SubAction, k8sClient, separator, hasHeader, outputFormat)
				if errExport != nil {
					fmt.Println(errExport.Error())
					return nil
				}
				if outputFile != "-" {
					sxUtils.WriteFile(outputFile, export)
					display.Info(fmt.Sprintf(msgExecExportRecordFile, args.SubAction, outputFile))
				} else {
					fmt.Println(export)
				}
			}

		case "describe":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			if args.IsHelp {
				sxQuotas.DisplayHelpCmdDescribe()
			} else {
				k8sClient := argParser.K8sclient.(*sxKCli.K8sClient)
				outputFormat := "detail"
				if argParser.Args.HasFlag("--format") {
					outputFormat = argParser.Args.GetFlagNextVal("--format")
					nsPos := argParser.Args.GetFlagPos("--format")
					argParser.Args.RemoveFlagFromPos(nsPos)
					argParser.Args.RemoveFlagFromPos(nsPos)
				}

				export, errExport := sxQuotas.RQDescriber(args.SubAction, k8sClient, outputFormat)
				if errExport != nil {
					fmt.Println(errExport.Error())
					return nil
				}
				display.Info(msgExecDescribeOutput)
				fmt.Println(export)
			}
		case "version":
			display.Debug(fmt.Sprintf(ddSeqStart, args.TopAction))
			DisplayVersion()
		default:
			display.Warning(fmt.Sprintf(ddSeqStart, args.TopAction))
			sxQuotas.DisplayHelpCmd()
		}
	}
	return nil
}
